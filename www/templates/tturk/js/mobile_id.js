/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.


// ...additional event handlers here...

 var tokenID = "";

        document.addEventListener("deviceready", function(){

			//alert('push');
            //Unregister the previous token because it might have become invalid. Unregister everytime app is started.
            window.plugins.pushNotification.unregister(unregistersuccessHandler, errorHandler);
            
                //register the user and get token
                window.plugins.pushNotification.register(
                successHandler,
                errorHandler,
                {
                    //senderID is the project ID
                    "senderID":"1034001420994",
                    //callback function that is executed when phone recieves a notification for this app
                    "ecb":"onNotification"
                });
                
           
        }, false);
 
        //app given permission to receive and display push messages in Android.
        function successHandler (result) {
           // alert('result = ' + result);
        }

		  function unregistersuccessHandler (result) {
           // alert('unregister result = ' + result);
        }
        
        //app denied permission to receive and display push messages in Android.
        function errorHandler (error) {
            alert('error = ' + error);
        }
        
        //App given permission to receive and display push messages in iOS
        function tokenHandler (result) {
            // Your iOS push server needs to know the token before it can push to this device
            // here is where you might want to send the token to your server along with user credentials.
            //alert('device token = ' + result);
            tokenID = result;
        }
        
        //fired when token is generated, message is received or an error occured.
        function onNotification(e) 
        {
            switch( e.event )
            {
                //app is registered to receive notification
                case 'registered':
                    if(e.regid.length > 0)
                    {
                        // Your Android push server needs to know the token before it can push to this device
            // here is where you might want to send the token to your server along with user credentials.
                       // alert('registration id = '+e.regid);
                        tokenID = e.regid;
						document.getElementById("test").value = e.regid;
						$('#test').val(e.regid);
                    }
                    break;

                case 'message':
                  //Do something with the push message. This function is fired when push message is received or if user clicks on the tile.
                  alert('message = '+e.message+' msgcnt = '+e.msgcnt);
                break;

                case 'error':
                  alert('GCM error = '+e.msg);
                break;

                default:
                  alert('An unknown GCM event has occurred');
                  break;
            }
        }

		//http://scoto.in/app/tturk/demo/api/receiver/newrawdata/receiver_id/" + tokenID + "?format=xml