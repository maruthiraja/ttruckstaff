//
//Welcome to app.js
//This is main application config of project. You can change a setting of :
//  - Global Variable
//  - Theme setting
//  - Icon setting
//  - Register View
//  - Spinner setting
//  - Custom style
//
//Global variable use for setting color, start page, message, oAuth key.
var db = null; //Use for SQLite database.
window.globalVariable = {
    //custom color style variable
    color: {
        appPrimaryColor: "",
        dropboxColor: "#017EE6",
        facebookColor: "#3C5C99",
        foursquareColor: "#F94777",
        googlePlusColor: "#D73D32",
        instagramColor: "#517FA4",
        wordpressColor: "#0087BE"
    },// End custom color style variable
    startPage: {
        url: "/app/home",//Url of start page.
        state: "app.home"//State name of start page.
    },
    message: {
        errorMessage: "Technical error please try again later." //Default error message.
    },
    oAuth: {
      dropbox: "your_api_key",//Use for Dropbox API clientID.
      facebook: "your_api_key",//Use for Facebook API appID.
      foursquare: "your_api_key", //Use for Foursquare API clientID.
      instagram: "your_api_key",//Use for Instagram API clientID.
      googlePlus: "your_api_key",//Use for Google API clientID.
    },
    adMob: "your_api_key" //Use for AdMob API clientID.
};// End Global variable


angular.module('starter', ['ionic','ngIOS9UIWebViewPatch', 'starter.controllers', 'starter.services', 'ngMaterial', 'ngMessages', 'ngCordova'])
    .run(function ($ionicPlatform, $cordovaSQLite, $rootScope, $ionicHistory, $state, $mdDialog, $mdBottomSheet) {

        //Create database table of contracts by using sqlite database.
        //Table schema :
        //Column	   Type	     Primary key
        //  id	        Integer	    Yes
        //  firstName	Text	    No
        //  lastName	Text	    No
        //  telephone	Text	    No
        //  email	    Text	    No
        //  note	    Text	    No
        //  createDate	DateTime	No
        //  age	        Integer	    No
        //  isEnable	Boolean	    No

        function initialSQLite() {
            db = window.cordova ? $cordovaSQLite.openDB("contract.db") : window.openDatabase("contract.db", "1.0", "IonicMaterialDesignDB", -1);
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS contracts " +
                "( id           integer primary key   , " +
                "  firstName    text                  , " +
                "  lastName     text                  , " +
                "  telephone    text                  , " +
                "  email        text                  , " +
                "  note         text                  , " +
                "  createDate   dateTime              , " +
                "  age          integer               , " +
                "  isEnable     Boolean)                ");
        };
        // End creating SQLite database table.

        // Create custom defaultStyle.
        function getDefaultStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : " + appPrimaryColor + " !important; " +
                "   border-style            : none;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        }// End create custom defaultStyle

        // Create custom style for product view.
        function getProductStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : " + appPrimaryColor + " !important;" +
                "   border-style            : none;" +
                "   background-image        : url('img/background_cover_pixels.png') !important;" +
                "   background-size         : initial !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        }// End create custom style for product view.

        // Create custom style for contract us view.
        function getContractUsStyle() {
            return "" +
                ".material-background-nav-bar { " +
                "   background-color        : transparent !important;" +
                "   border-style            : none;" +
                "   background-image        : none !important;" +
                "   background-position-y   : 4px !important;" +
                "   background-size         : initial !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                     : " + appPrimaryColor + " !important;" +
                "}";
        } // End create custom style for contract us view.

        // Create custom style for Social Network view.
        function getSocialNetworkStyle(socialColor) {
            return "" +
                ".material-background-nav-bar {" +
                "   background              : " + socialColor + " !important;" +
                "   border-style            : none;" +
                "} " +
                "md-ink-bar {" +
                "   color                   : " + socialColor + " !important;" +
                "   background              : " + socialColor + " !important;" +
                "}" +
                "md-tab-item {" +
                "   color                   : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-left .md-half-circle {" +
                "   border-left-color       : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-left .md-half-circle, md-progress-circular.md-warn .md-inner .md-right .md-half-circle {" +
                "    border-top-color       : " + socialColor + " !important;" +
                "}" +
                " md-progress-circular.md-warn .md-inner .md-gap {" +
                "   border-top-color        : " + socialColor + " !important;" +
                "   border-bottom-color     : " + socialColor + " !important;" +
                "}" +
                "md-progress-circular.md-warn .md-inner .md-right .md-half-circle {" +
                "  border-right-color       : " + socialColor + " !important;" +
                " }" +
                ".spinner-android {" +
                "   stroke                  : " + socialColor + " !important;" +
                "}" +
                ".md-primary-color {" +
                "   color                   : " + socialColor + " !important;" +
                "}" +
                "a.md-button.md-primary, .md-button.md-primary {" +
                "   color                   : " + socialColor + " !important;" +
                "}";
        }// End create custom style for Social Network view.


        function initialRootScope() {
            $rootScope.appPrimaryColor = appPrimaryColor;// Add value of appPrimaryColor to rootScope for use it to base color.
            $rootScope.isAndroid = ionic.Platform.isAndroid();// Check platform of running device is android or not.
            $rootScope.isIOS = ionic.Platform.isIOS();// Check platform of running device is ios or not.
        };

        function hideActionControl() {
            //For android if user tap hardware back button, Action and Dialog should be hide.
            $mdBottomSheet.cancel();
            $mdDialog.cancel();
        };


        // createCustomStyle will change a style of view while view changing.
        // Parameter :
        // stateName = name of state that going to change for add style of that page.
        function createCustomStyle(stateName) {
            var customStyle =
                ".material-background {" +
                "   background-color          : " + appPrimaryColor + " !important;" +
                "   border-style              : none;" +
                "}" +
                ".spinner-android {" +
                "   stroke                    : " + appPrimaryColor + " !important;" +
                "}";

            switch (stateName) {
                case "app.productList" :
                case "app.productDetail":
                case "app.productCheckout":
                case "app.clothShop" :
                case "app.catalog" :
                    customStyle += getProductStyle();
                    break;
                case "app.dropboxLogin" :
                case "app.dropboxProfile":
                case "app.dropboxFeed" :
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.dropboxColor);
                    break;
                case "app.facebookLogin" :
                case "app.facebookProfile":
                case "app.facebookFeed" :
                case "app.facebookFriendList":
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.facebookColor);
                    break;
                case "app.foursquareLogin" :
                case "app.foursquareProfile":
                case "app.foursquareFeed" :
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.foursquareColor);
                    break;
                case "app.googlePlusLogin" :
                case "app.googlePlusProfile":
                case "app.googlePlusFeed" :
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.googlePlusColor);
                    break;
                case "app.instagramLogin" :
                case "app.instagramProfile":
                case "app.instagramFeed" :
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.instagramColor);
                    break;
                case "app.wordpressLogin" :
                case "app.wordpressFeed":
                case "app.wordpressPost" :
                    customStyle += getSocialNetworkStyle(window.globalVariable.color.wordpressColor);
                    break;
                case "app.contractUs":
                    customStyle += getContractUsStyle();
                    break;
                default:
                    customStyle += getDefaultStyle();
                    break;
            }
            return customStyle;
        }// End createCustomStyle

        // Add custom style while initial application.
        $rootScope.customStyle = createCustomStyle(window.globalVariable.startPage.state);

        $ionicPlatform.ready(function () {
            ionic.Platform.isFullScreen = true;

           if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            //initialSQLite();
            initialRootScope();

            //Checking if view is changing it will go to this function.
            $rootScope.$on('$ionicView.beforeEnter', function () {
                //hide Action Control for android back button.
                hideActionControl();
                // Add custom style ti view.
                $rootScope.customStyle = createCustomStyle($ionicHistory.currentStateName());
            });
        });

    })

  .filter('htmlToPlaintext', function() {
    return function(text) {
		console.log(text);
      //return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
	  //return $(text).text();
	  return text;
    };
  })
  /*
  .filter('renderHTMLCorrectly', function($sce)
{
	return function(stringToParse)
	{
		return $sce.trustAsHtml(stringToParse);
	}
})*/

    .config(function ($ionicConfigProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider, $mdIconProvider, $mdColorPalette, $mdIconProvider) {


        // Use for change ionic spinner to android pattern.
        $ionicConfigProvider.spinner.icon("android");
        $ionicConfigProvider.views.swipeBackEnabled(false);

		$ionicConfigProvider.navBar.alignTitle('center')

        // mdIconProvider is function of Angular Material.
        // It use for reference .SVG file and improve performance loading.
        $mdIconProvider
            .icon('facebook', 'img/icons/facebook.svg')
            .icon('twitter', 'img/icons/twitter.svg')
            .icon('mail', 'img/icons/mail.svg')
            .icon('message', 'img/icons/message.svg')
            .icon('share-arrow', 'img/icons/share-arrow.svg')
			.icon('wrong', 'img/icons/wrong.svg')
			.icon('checked', 'img/icons/checked.svg')
            .icon('more', 'img/icons/more_vert.svg')
			.icon('half', 'img/icons/half.svg');

        //mdThemingProvider use for change theme color of Ionic Material Design Application.
        /* You can select color from Material Color List configuration :
         * red
         * pink
         * purple
         * purple
         * deep-purple
         * indigo
         * blue
         * light-blue
         * cyan
         * teal
         * green
         * light-green
         * lime
         * yellow
         * amber
         * orange
         * deep-orange
         * brown
         * grey
         * blue-grey
         */
        //Learn more about material color patten: https://www.materialpalette.com/
        //Learn more about material theme: https://material.angularjs.org/latest/#/Theming/01_introduction
        $mdThemingProvider
            .theme('default')
            .primaryPalette('pink')
            .accentPalette('red');

        appPrimaryColor = $mdColorPalette[$mdThemingProvider._THEMES.default.colors.primary.name]["500"]; //Use for get base color of theme.

        //$stateProvider is using for add or edit HTML view to navigation bar.
        //
        //Schema :
        //state_name(String)      : Name of state to use in application.
        //page_name(String)       : Name of page to present at localhost url.
        //cache(Bool)             : Cache of view and controller default is true. Change to false if you want page reload when application navigate back to this view.
        //html_file_path(String)  : Path of html file.
        //controller_name(String) : Name of Controller.
        //
        //Learn more about ionNavView at http://ionicframework.com/docs/api/directive/ionNavView/
        //Learn more about  AngularUI Router's at https://github.com/angular-ui/ui-router/wiki
        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/tturk/menu.html",
                controller: 'menuCtrl'
            })
			 .state('app.login', {
                url: "/login",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/login.html",
						controller: 'loginCtrl'
                    }
                }
            })
			 .state('app.home', {
                url: "/home",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/home.html",
						controller: 'homeCtrl'
                    }
                }
            })
			.state('app.stafflogin', {
                url: "/stafflogin",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/stafflogin.html",
						controller: 'staffloginCtrl'
                    }
                }
            })
			
            .state('app.dashboard', {
                url: "/dashboard",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/dashboard.html",
                        controller: 'dashbCtrl'
                    }
                }
			})

			.state('app.events', {
                url: "/events",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/events.html",
                        controller: 'eventsCtrl'
                    }
                }
			})

			.state('app.achievements', {
                url: "/achievements",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/achievements.html",
                        controller: 'achievementsCtrl'
                    }
                }
			})

			.state('app.circulars', {
                url: "/circulars",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/circulars.html",
                        controller: 'circularsCtrl'
                    }
                }
			})

            .state('app.noticeboard', {
                url: "/noticeboard",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/notice_board.html",
                        controller: 'noticeboardCtrl'
                    }
                }
            })
          

			.state('app.library', {
                url: "/library",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/library.html",
                        controller: 'libraryCtrl'
                    }
                }
			})

			.state('app.book_issued', {
                url: "/book_issued",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_issued.html",
                        controller: 'book_issuedCtrl'
                    }
                }
			})

			.state('app.book_returned', {
                url: "/book_returned",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_returned.html",
                        controller: 'book_returnedCtrl'
                    }
                }
			})

			.state('app.book_request', {
                url: "/book_request",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_request.html",
                        controller: 'book_requestCtrl'
                    }
                }
			})

			.state('app.book_reserve', {
                url: "/book_reserve",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_reserve.html",
                        controller: 'book_reserveCtrl'
                    }
                }
			})

			.state('app.book_search', {
                url: "/book_search",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_search.html",
                        controller: 'book_searchCtrl'
                    }
                }
			})

			.state('app.book_request_list', {
                url: "/book_request_list",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/book_request_list.html",
                        controller: 'book_request_listCtrl'
                    }
                }
			})


			
         /*   .state('app.selectclass', {
                url: "/selectclass",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/selectclass.html",
                        controller: 'selectclassCtrl'
                    }
                }
			})
*/
			.state('app.timetable', {
                url: "/timetable",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/timetable.html",
                        controller: "timetableCtrl"
                    }
                }
            })

			.state('app.mark_attendance', {
                url: "/mark_attendance",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/mark_attendance.html",
						controller: 'mark_attendanceCtrl'
                    }
                }
            })
            .state('app.automatic_attendance', {
                url: "/automatic_attendance",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/automatic_attendance.html",
						controller: 'automaticCtrl'
                    }
                }
            })
            
            .state('app.hourly_attendance', {
                url: "/hourly_attendance",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/hourly_attendance.html",
						controller: 'hourly_attendanceCtrl'
                    }
                }
            })

            .state('app.mark', {
                url: "/mark",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/mark.html",
						controller: 'markCtrl'
                    }
                }
            })


			.state('app.view_attendance', {
                url: "/view_attendance",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/view_attendance.html",
						controller: 'view_attendanceCtrl'
                    }
                }
            })

			.state('app.edit_attendance', {
                url: "/edit_attendance",
				cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/edit_attendance.html",
						controller: 'edit_attendanceCtrl'
                    }
                }
            })

			.state('app.assignment', {
                url: "/assignment",
				 cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/assignment.html",
						controller: 'assignmentCtrl'
                    }
                }
            })

           .state('app.view_assignment', {
                url: "/view_assignment",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/view_assignment.html",
						controller: 'view_assignmentCtrl'
                    }
                }
            })

			.state('app.search_assignment', {
                url: "/search_assignment",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/search_assignment.html",
						controller: 'search_assignmentCtrl'
                    }
                }
            })

           .state('app.leavedetails', {
                url: "/leavedetails",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/leavedetails.html",
						controller: 'leavedetailsCtrl'
                    }
                }
            })

			.state('app.logout', {
                url: "/logout",
                views: {
                    'menuContent': {
                        templateUrl: "templates/tturk/login.html",
                        controller: 'logoutCtrl'
                    }
                }
		
            });// End $stateProvider

        /*
            var authID = localStorage.getItem("teacherID");	
		//alert(authID); 
		if(authID == null||authID == '')	
        {	//	alert('if'+authID); 
        $urlRouterProvider.otherwise(window.globalVariable.startPage.url);
		
			//alert('if1'+authID); 
		}else{
            //alert('else'+authID); 
            var baseURL = localStorage.getItem("baseurl");	
            var teacherID = localStorage.getItem("teacherID");	
            console.log("BASE URL "+ baseURL);
            console.log("*********");
            console.log("TEACHER ID "+ teacherID);
        
              $urlRouterProvider.otherwise('/app/dashboard');
		
        }
        */
			//alert(window.globalVariable.startPage.url);
				//alert('else'+authID); 
        //Use $urlRouterProvider.otherwise(Url);
       $urlRouterProvider.otherwise(window.globalVariable.startPage.url);

    });
