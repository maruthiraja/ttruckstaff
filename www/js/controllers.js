var appControllers = angular.module('starter.controllers', ['ngCordova', 'textAngular']); // Use for all controller of application.
var appServices = angular.module('starter.services', []);// Use for all service of application.
appControllers.run(function ($ionicPlatform, $cordovaNetwork, $rootScope) {
    //alert("hi hello1");
    try {

        FCMPlugin.getToken(function (token) {
            //this is the fcm token which can be used
            //to send notification to specific device 
            //alert(token);

        });
        //FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
        //Here you define your application behaviour based on the notification data.
        FCMPlugin.onNotification(function (data) {
            //alert("test "+data.message);
            //alert("test "+data['message']);
            //console.log(data);
            //data.wasTapped == true means in Background :  Notification was received on device tray and tapped by the user.
            //data.wasTapped == false means in foreground :  Notification was received in foreground. Maybe the user needs to be notified.
            if (data.wasTapped) {
                //Notification was received on device tray and tapped by the user.
                //alert(JSON.stringify(data));
            } else {
                //Notification was received in foreground. Maybe the user needs to be notified.
                // alert(JSON.stringify(data));
            }
        });


    } catch (err) {
        //alert("receivedEvent error : " + err);
    }


});

/*
appControllers.filter('renderHTMLCorrectly', function($sce)
{
	return function(stringToParse)
	{
		console.log(stringToParse);
		console.log($sce.trustAsHtml(stringToParse));
		return $sce.trustAsHtml(stringToParse);
	}
});
*/
// Controller of home page.
appControllers.controller('homeCtrl', function ($scope, $rootScope, $state, $stateParams, $ionicHistory, $mdDialog, $timeout, $http) {
	$scope.isAnimated = $stateParams.isAnimated;
	//$rootScope.value='Its parent';
	//$state.reload();
	/*
	$scope.navigateTo = function (stateName) {
        $timeout(function () {
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: false,
                    disableBack: true
                }); 
                $state.go(stateName);
            }
        }, ($scope.isAnimated  ? 300 : 0));
    }; // End of navigateTo.
*/
	//localStorage.setItem("baseurl","https://demoschool.tturk.in")
	$scope.baseurl = localStorage.getItem("baseurl");
	console.log(" BASE URL " + $scope.baseurl)
	//alert($scope.baseurl);
	if (($scope.baseurl != '') && ($scope.baseurl != null)) {
		var teacherID = localStorage.getItem("teacherID");
		if ((teacherID != '') && (teacherID != null)) {
			//alert('home refresh'); 
			//alert('home ctrl');
		$timeout(function () {
			//alert('home refresh'); 
			$state.go("app.dashboard");
		}, 1000);
	
		}else{
			$timeout(function () {
				$state.go("app.login");
			}, 1000);
		}
	
	}
	//$state.go("app.login");


	$scope.loginsubmit = function (schoolid, studentid) {

		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://demo.tturk.in/api/parentAPI/getstudentdata/studentid/" + studentid;

			var url ="http://admin.tturk.in/admin/api/register/getappschoolbase/schoolcode/" + schoolid;
			$http.get(url, { params: {"key1": "value1"} })
			.success(function (data) {
				//alert(data);
				console.log(data);
				localStorage.setItem("schoolcode", schoolid);
					localStorage.setItem("baseurl", data[0]['url']);
					localStorage.setItem("schoolname", data[0]['name']);

					window.localStorage.setItem('schoolcode', schoolid);
					window.localStorage.setItem('baseurl', data[0]['url']);
					window.localStorage.setItem('schoolname', data[0]['name']);

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: "Thank you...!!!",
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						//$scope.reset();
						$state.go("app.login");
					});			})
			.error(function (data) {
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'confirm-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Invalid School ID...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$state.go("app.home");
				});
				console.log(data);
			});

	}

}); // End of  home controller.

appControllers.controller('loginCtrl', function ($scope, $rootScope, $timeout, $state, $stateParams, $ionicHistory, $mdDialog, $http) {
	//alert('start');
	//console.log($rootScope.value);
	//$state.reload();
	//$scope.isAnimated is the variable that use for receive object data from state params.
	//For enable/disable row animation.
	$scope.isAnimated = $stateParams.isAnimated;

	// navigateTo is for navigate to other page 
	// by using targetPage to be the destination state. 
	// Parameter :  
	// stateNames = target state to go.
	/* $scope.navigateTo = function (stateName) {
		 $timeout(function () {
			 if ($ionicHistory.currentStateName() != stateName) {
				 $ionicHistory.nextViewOptions({
					 disableAnimate: false,
					 disableBack: true
				 });
				 $state.go(stateName);
			 }
		 }, ($scope.isAnimated  ? 300 : 0));
	 }; // End of navigateTo.
	 */
	var teacherID = localStorage.getItem("teacherID");
	if ((teacherID != '') && (teacherID != null)) {
		//alert('home refresh'); 
		$state.go("app.dashboard");
		//alert('home ctrl');

	}

	/*
	$scope.username = localStorage.getItem("username");
	if($scope.username != null)
	{
	alert($scope.username);
	$state.go("app.dashboard");
	}
	else
	{
		*/
	//$state.go("app.login");

	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/editattendancepermission";
	console.log(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		})
		.error(function (data) {
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		});
	// Get session end


	$scope.schoolname = localStorage.getItem("schoolname");

	$scope.login = function () {
		$state.go("app.stafflogin");
	};
	$scope.login1 = function () {
		//alert('login');
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		var url = $scope.baseurl + "/api/staffAPI/teacherlogin/card/ad01e96c";
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				var teacherID = data[0]['teacherID'];
				var name = data[0]['name'];
				var staffphoto = data[0]['photo'];
				//alert(teacherID);

				localStorage.setItem("teacherID", teacherID);
				localStorage.setItem("name", name);
				localStorage.setItem("staffphoto", staffphoto);




				$scope.baseurl = localStorage.getItem("baseurl");
				var t_url = $scope.baseurl + "/api/staffAPI/getclassteacher/id/" + teacherID;
				console.log(t_url);
				$http.get(t_url, { params: { "key1": "value1", "key2": "value2" } })
					.success(function (data) {
						var t_classesID = data[0]['classesID'];
						var t_sectionID = data[0]['sectionID'];
						localStorage.setItem("classID", t_classesID);
						localStorage.setItem("sectionID", t_sectionID);
						//$state.reload();
						$state.go("app.dashboard");
					})
					.error(function (data) {
						$state.go("app.dashboard");
					});






			})
			.error(function (data) {
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'confirm-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Teacher Access Denied...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$state.go("app.login");
				});
			});

		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		//	$state.go("app.dashboard");
	}


}); // End of  login controller.


appControllers.controller('staffloginCtrl', function ($scope, $rootScope, $timeout, $state, $stateParams, $ionicHistory, $mdDialog, $http) {
	//alert('start');
	//$state.reload();

	$scope.isAnimated = $stateParams.isAnimated;

	$scope.navigateTo = function (stateName) {
		$timeout(function () {
			if ($ionicHistory.currentStateName() != stateName) {
				$ionicHistory.nextViewOptions({
					disableAnimate: false,
					disableBack: true
				});
				$state.go(stateName);
			}
		}, ($scope.isAnimated ? 300 : 0));
	}; // End of navigateTo.


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/editattendancepermission";
	console.log(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		})
		.error(function (data) {
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		});
	// Get session end

	$scope.schoolname = localStorage.getItem("schoolname");

	$scope.stafflogin = function (username, password) {
		//alert(username);alert(password);
		if (username != '' || username != NULL) {

        FCMPlugin.getToken(function (token) {
            //this is the fcm token which can be used
            //to send notification to specific device 
            //alert(token);
			
	localStorage.setItem("fcmToken", token);
        });
			$scope.baseurl = localStorage.getItem("baseurl");
			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
					var token=localStorage.getItem("fcmToken");
console.log("FCM TOKEN"+ token);
			var url = $scope.baseurl + "/api/staffAPI/stafflogin/username/" + username + "/password/" + password+ "/token/" + token;
			console.log(url);
			$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					var teacherID = data[0]['teacherID'];
					var name = data[0]['name'];
					var staffphoto = data[0]['photo'];
					//alert(teacherID);
					var subjectID = data[0]['subjectID'];
					localStorage.setItem("subjectID", subjectID);
					localStorage.setItem("teacherID", teacherID);
					localStorage.setItem("username", username);
					localStorage.setItem("name", name);
					localStorage.setItem("staffphoto", staffphoto);




					$scope.baseurl = localStorage.getItem("baseurl");
					var t_url = $scope.baseurl + "/api/staffAPI/getclassteacher/id/" + teacherID;
					console.log(t_url);
					$http.get(t_url, { params: { "key1": "value1", "key2": "value2" } })
						.success(function (data) {
							var t_classesID = data[0]['classesID'];
							var t_sectionID = data[0]['sectionID'];
							localStorage.setItem("classID", t_classesID);
							localStorage.setItem("sectionID", t_sectionID);
							//$state.reload();
							$state.go("app.dashboard");
						})
						.error(function (data) {
							$state.go("app.dashboard");
						});

				})
				.error(function (data) {
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: "Teacher Access Denied...!!!",
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						//$state.go("app.login");
					});
				});
		}
		else {
		}
		$ionicHistory.nextViewOptions({
			disableBack: true
		});

		//	$state.go("app.dashboard");
	}

}); // End of  stafflogin controller.


// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('menuCtrl', function ($scope, $timeout, $mdUtil, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect, $http) {

	$scope.toggleLeft = buildToggler('left');

	$scope.teacherID = localStorage.getItem("teacherID");
	$scope.name = localStorage.getItem("name");
	$scope.schoolname = localStorage.getItem("schoolname");
	$scope.staffphoto = localStorage.getItem("staffphoto");
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.editoption = localStorage.getItem("editoption");
	//$scope.editoption = 0;


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/editattendancepermission";
	console.log(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		})
		.error(function (data) {
			var editoption = data['option'];
			localStorage.setItem("editoption", editoption);
			$scope.editoption = editoption;
		});
	// Get session end

	if ($scope.editoption == 0) {
		$('.editcss').css('display', 'none');
	}
	else {
		$('.editcss').css('display', 'block');
	}


	// buildToggler is for create menu toggle.
	// Parameter :  
	// navID = id of navigation bar.
	function buildToggler(navID) {
		var debounceFn = $mdUtil.debounce(function () {
			$mdSidenav(navID).toggle();
			$scope.teacherID = localStorage.getItem("teacherID");
			$scope.name = localStorage.getItem("name");
			$scope.schoolname = localStorage.getItem("schoolname");
			$scope.staffphoto = localStorage.getItem("staffphoto");
			$scope.baseurl = localStorage.getItem("baseurl");
		}, 0);
		return debounceFn;
	};// End buildToggler.

	// navigateTo is for navigate to other page 
	// by using targetPage to be the destination state. 
	// Parameter :  
	// stateNames = target state to go
	$scope.navigateTo = function (stateName) {
		$timeout(function () {
			$mdSidenav('left').close();
			if ($ionicHistory.currentStateName() != stateName) {
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});
				$scope.teacherID = localStorage.getItem("teacherID");
				$scope.vendorname = localStorage.getItem("vendorname");
				$state.go(stateName);
			}
		}, ($scope.isAndroid == false ? 300 : 0));
	};// End navigateTo.

	//closeSideNav is for close side navigation
	//It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
	//When user swipe or drag md-sidenav to left side
	$scope.closeSideNav = function () {
		$mdSidenav('left').close();
		$scope.teacherID = localStorage.getItem("teacherID");
		$scope.name = localStorage.getItem("name");
		$scope.schoolname = localStorage.getItem("schoolname");
	};
	//End closeSideNav

	$ionicPlatform.registerBackButtonAction(function () {

		if ($mdSidenav("left").isOpen()) {
			//If side navigation is open it will close and then return
			$mdSidenav('left').close();
		}
		else if (jQuery('md-bottom-sheet').length > 0) {
			//If bottom sheet is open it will close and then return
			$mdBottomSheet.cancel();
		}
		else if (jQuery('[id^=dialog]').length > 0) {
			//If popup dialog is open it will close and then return
			$mdDialog.cancel();
		}
		else if (jQuery('md-menu-content').length > 0) {
			//If md-menu is open it will close and then return
			$mdMenu.hide();
		}
		else if (jQuery('md-select-menu').length > 0) {
			//If md-select is open it will close and then return
			$mdSelect.hide();
		}

		else {
			// Check for the current state that not have previous state.
			// It will show $mdDialog to ask for Confirmation to close the application.

			if ($ionicHistory.backView() == null) {

				//Check is popup dialog is not open.
				if (jQuery('[id^=dialog]').length == 0) {



					// mdDialog for show $mdDialog to ask for
					// Confirmation to close the application.

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: null,
						locals: {
							displayOption: {
								title: "Confirmation",
								content: "Do you want to close the application?",
								ok: "Confirm",
								cancel: "Cancel"
							}
						}
					}).then(function () {
						//If user tap Confirm at the popup dialog.
						//Application will close.
						ionic.Platform.exitApp();
					}, function () {
						// For cancel button actions.
					}); //End mdDialog


				}
			}
			else {

				$st_name = $state.current.name;
				if ($st_name == "app.dashboard") {
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: null,
						locals: {
							displayOption: {
								title: "Confirmation",
								content: "Do you want to close the application?",
								ok: "Confirm",
								cancel: "Cancel"
							}
						}
					}).then(function () {
						//If user tap Confirm at the popup dialog.
						//Application will close.
						ionic.Platform.exitApp();
					}, function () {
						// For cancel button actions.
					}); //End mdDialog
				}
				else {
					//Go to the view of lasted state.
					$ionicHistory.goBack();

				}
			}
		}

	}, 100);
	//End of $ionicPlatform.registerBackButtonAction
}); // End of menu toggle controller.


// Controller of dashboard page.
appControllers.controller('dashbCtrl', function ($scope, $rootScope, $state, $mdDialog, $http) {
	//alert('dashbCtrl');
	//$state.reload();

	//localStorage.setItem("classID", "2"); 
	//localStorage.setItem("sectionID", "10");

	$scope.reloadPage = function () {
		$state.reload();
	}

	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
			//localStorage.setItem("allclass", data);
		})
		.error(function (data) {
			//$(".dash_content").css('display','block');
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};


	$scope.teacherID = localStorage.getItem("teacherID");
	$scope.teachername = localStorage.getItem("name");
	//$scope.classID = '3';
	//$scope.sectionID = '11';
	//$scope.classname = '3';
	//$scope.sectionname = 'A';
	var date = new Date();
	var yr = date.getFullYear(),
		month = date.getMonth() + 1,
		months = month < 10 ? '0' + month : month,
		day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

	var pre_date = date.getDate() - 1 < 10 ? '0' + date.getDate() - 1 : date.getDate() - 1;
	var date_assign = yr + '-' + months + '-' + pre_date;

	$scope.date = day + '-' + months + '-' + yr;
	//$scope.classID = localStorage.getItem("classID");
	//$scope.sectionID = localStorage.getItem("sectionID");
	// Get Class name based on Class ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/classid/" + $scope.classID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.classname = data[0].classes;
			localStorage.setItem("classname", $scope.classname);
		})

	// Get Section name based on Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.sectionname = data[0].section;
			localStorage.setItem("sectionname", $scope.sectionname);
		})
	//alert($scope.classID);
	//alert($scope.sectionID);
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstrengthdetails/classid/" + classID + "/sectionid/" + sectionID;
	//alert(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.totalstrength = data[0].total;
			$scope.presentstrength = data[0].present;
			$scope.absentstrength = data[0].absent;
			//localStorage.setItem("classname", $scope.classname);
		})
		.error(function (data) {
			$scope.totalstrength = '-';
			$scope.presentstrength = '-';
			$scope.absentstrength = '-';
		});


	$scope.call_events = function () {
		$state.go("app.events");
	};

	$scope.call_achievements = function () {
		$state.go("app.achievements");
	};

	$scope.call_circulars = function () {
		$state.go("app.circulars");
	};

	$scope.call_assignments = function () {
		$state.go("app.view_assignment");
	};

	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + classID + "/sectionid/" + sectionID + "/date/" + date_assign;

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.datas = data[0];
		})
		.error(function (data) {

		});
	// For simulator

}); // End of  dashboard controller.



// Controller of leavedetailsCtrl page.
appControllers.controller('leavedetailsCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $ionicModal, $state, $stateParams) {



	$scope.reloadPage = function () {
		$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {//alert("1q");
		$scope.teacherID = localStorage.getItem("teacherID");
		$scope.teachername = localStorage.getItem("name");
		//$scope.classID = '3';
		//$scope.sectionID = '11';
		//$scope.classname = '3';
		//$scope.sectionname = 'A';
		var date = new Date();
		var yr = date.getFullYear(),
			month = date.getMonth() + 1,
			months = month < 10 ? '0' + month : month,
			day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
		var date_assign = yr + '-' + months + '-' + day;
		$scope.date = day + '-' + months + '-' + yr;
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$scope.popup = function (date, notification) {
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'alert-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: date,
						content: notification,
						cancel: "Ok"
					}
				}
			}).then(function () {
				//$state.reload();
			});
		}

		//alert($scope.parentID);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://webapp.scoto.in/tturk/api/staffAPI/leavedetails/classID/2/sectionID/10?key1=value1&key2=value2";
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/leavedetails/classID/" + $scope.classID + "/sectionID/" + $scope.sectionID;
		//alert(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.datas = data;
				$scope.msg = "0";

			})
			.error(function (data) {
				$scope.msg = "1";
				//alert('error');
			});


		//Default value of radio data.
		//$scope.radioData = {fruit: 1};
		// For show show List Bottom Sheet.
		$scope.showListBottomSheet = function ($event) {
			$mdBottomSheet.show({
				templateUrl: 'ui-list-bottom-sheet-template',
				targetEvent: $event,
				scope: $scope.$new(false),
			});
		};// End of showListBottomSheet.

		// For close list bottom sheet.
		$scope.closeListBottomSheet = function () {
			$mdBottomSheet.hide();
		} // End of closeListBottomSheet.


		// For show Grid Bottom Sheet.
		$scope.showGridBottomSheet = function ($event) {
			$mdBottomSheet.show({
				templateUrl: 'ui-grid-bottom-sheet-template',
				targetEvent: $event,
				scope: $scope.$new(false),
			});
		};// End of showGridBottomSheet.
		//Default value of radio data.
		$ionicModal.fromTemplateUrl('templates/leave.html', {
			scope: $scope
		}).then(function (modal) {
			$scope.modal = modal;
		});
		$scope.show = function (id) {
			//alert(id);
			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			var url = $scope.baseurl + "/api/staffAPI/getsingleleave/id/" + id + "?format=json";
			$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					//alert(data);
					console.log(data);
					$scope.single_data = data[0];
				})
				.error(function (data) {
					$scope.single_data = '';
					console.log(data);
				});
			$scope.modal.show();

		};


		$scope.leavefn = function (id, status) {
			$scope.modal.hide();
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'confirm-dialog.html',
				targetEvent: null,
				locals: {
					displayOption: {
						title: "Confirmation",
						content: "Are you sure?",
						ok: "Confirm",
						cancel: "Cancel"
					}
				}
			}).then(function () {
				$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
				$scope.baseurl = localStorage.getItem("baseurl");
				var url = $scope.baseurl + "/api/staffAPI/leavestatus/id/" + id + "/status/" + status + "/reject_details/0?format=json";
				console.log(url);
				$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
					.success(function (data) {
						//alert(data);
						console.log(data);
						$mdDialog.show({
							controller: 'DialogController',
							templateUrl: 'alert-dialog.html',
							targetEvent: '',
							locals: {
								displayOption: {
									title: "Leave Status Updated...!!!",
									content: "",
									ok: "OK"
								}
							}
						}).then(function () {
							$scope.loadData();
							//
						});
					})
					.error(function (data) {
						console.log(data);
					});
			}, function () {
				// For cancel button actions.

				$scope.loadData();
			}); //End mdDialog
		};


		$scope.rejectleavefn = function (id, status, reject_details) {
			if (reject_details == null || reject_details == "undefined" || reject_details == '') {
				//alert('if');
			}
			else {
				$scope.modal.hide();
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'confirm-dialog.html',
					targetEvent: null,
					locals: {
						displayOption: {
							title: "Confirmation",
							content: "Are you sure?",
							ok: "Confirm",
							cancel: "Cancel"
						}
					}
				}).then(function () {
					$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
					$scope.baseurl = localStorage.getItem("baseurl");
					var url = $scope.baseurl + "/api/staffAPI/leavestatus/id/" + id + "/status/" + status + "/reject_details/" + reject_details + "?format=json";
					console.log(url);
					$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
						.success(function (data) {
							//alert(data);
							console.log(data);
							$mdDialog.show({
								controller: 'DialogController',
								templateUrl: 'alert-dialog.html',
								targetEvent: '',
								locals: {
									displayOption: {
										title: "Leave Status Updated...!!!",
										content: "",
										ok: "OK"
									}
								}
							}).then(function () {
								$scope.loadData();
								//
							});
						})
						.error(function (data) {
							console.log(data);
						});
				}, function () {
					// For cancel button actions.

					$scope.loadData();
				}); //End mdDialog
			}
			$('.reject_box').css('display', 'block');
		};

		$('.reject_box').css('display', 'none');
	}
	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  notification controller.




// Controller of notice board page.
appControllers.controller('noticeboardCtrl', function ($scope, $http, $mdDialog, $ionicModal, $state, $stateParams) {

	$scope.teacherID = localStorage.getItem("teacherID");


	$scope.call_events = function () {
		$state.go("app.events");
	};

	$scope.call_achievements = function () {
		$state.go("app.achievements");
	};

	$scope.call_circulars = function () {
		$state.go("app.circulars");
	};

	$scope.call_assignments = function () {
		$state.go("app.view_assignment");
	};

	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstrengthdetails/classid/" + classID + "/sectionid/" + sectionID;
	//alert(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.totalstrength = data[0].total;
			$scope.presentstrength = data[0].present;
			$scope.absentstrength = data[0].absent;
			//localStorage.setItem("classname", $scope.classname);
		})
		.error(function (data) {
			$scope.totalstrength = '-';
			$scope.presentstrength = '-';
			$scope.absentstrength = '-';
		});

}); // End of  notice board controller.



// Controller of notice board page.
appControllers.controller('markCtrl', function ($scope, $http, $mdDialog, $ionicModal, $state, $stateParams) {

	$scope.teacherID = localStorage.getItem("teacherID");


	$scope.manual = function () {
		$state.go("app.mark_attendance");
	};

	$scope.automatic = function () {
		$state.go("app.automatic_attendance");
	};


}); // End of  notice board controller.



// Controller of events page.
appControllers.controller('eventsCtrl', function ($scope, $http, $mdDialog, $ionicModal, $state, $stateParams) {
	$scope.teacherID = localStorage.getItem("teacherID");
	$scope.teachername = localStorage.getItem("name");
	var date = new Date();
	var yr = date.getFullYear(),
		month = date.getMonth() + 1,
		months = month < 10 ? '0' + month : month,
		day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
	var date_assign = yr + '-' + months + '-' + day;
	$scope.date = day + '-' + months + '-' + yr;
	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.popup = function (date, notification, name, time) {
		var datetimepop = date + ' ' + time;
		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'alert-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: datetimepop,
					content: notification,
					cancel: "Ok"
				}
			}
		}).then(function () {
			//$state.reload();
		});
	}


	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getevents?key1=value1&key2=value2";
	//var url ="http://webapp.scoto.in/tturk/api/staffAPI/getevents?key1=value1&key2=value2";
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			//alert("SUCCESS");
			console.log(data);
			$scope.datas = data;

		})
		.error(function (data) {
		});

}); // End of  events controller.


// Controller of achievements page.
appControllers.controller('achievementsCtrl', function ($scope, $http, $mdDialog, $ionicModal, $state, $stateParams) {

	$scope.teacherID = localStorage.getItem("teacherID");



	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getachievement?key1=value1&key2=value2";
	//var url ="http://webapp.scoto.in/tturk/api/staffAPI/getachievement?key1=value1&key2=value2";
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			//alert("SUCCESS");
			console.log(data);
			$scope.datas = data;

		})
		.error(function (data) {
		});


	$scope.popup = function (date, notification, title) {
		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'alert-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: title,
					content: notification,
					ok: "Ok"
				}
			}
		}).then(function () {
			//$state.reload();
		});
	}


}); // End of achievements controller.


// Controller of circular page.
appControllers.controller('circularsCtrl', function ($scope, $http, $mdDialog, $ionicModal, $state, $stateParams) {

	$scope.teacherID = localStorage.getItem("teacherID");

	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getcircular/teacherID/" + $scope.teacherID + "?key1=value1&key2=value2";
	//var url ="http://webapp.scoto.in/tturk/api/staffAPI/getcircular?key1=value1&key2=value2";
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			//alert("SUCCESS");
			console.log(data);
			$scope.datas = data;

		})
		.error(function (data) {
		});


	$scope.popup = function (date, notification) {
		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'alert-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: date,
					content: notification,
					ok: "Ok"
				}
			}
		}).then(function () {
			//$state.reload();
		});
	}


}); // End of circular controller.



// Controller of mark attendance page.
//hourly attendance
appControllers.controller('hourly_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http) {

	$scope.reloadPage = function () { $state.reload(); }
	$scope.date = new Date();
	localStorage.setItem("selectedSession", null);
	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");
	$scope.selectSpecific_subject = localStorage.getItem("selectSpecific_subject");
	$scope.selectPeriod = localStorage.getItem("selectPeriod");
	$scope.isDisabled = true;
	$('.attn_warn').html('');
	// Get check_attn_mark_per_day start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/attn_mark_per_day";
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var setting = data[0]['setting'];
			$scope.attendanceperDay = setting;
			localStorage.setItem("attn_typ", setting);
			if (setting == '2') {
				$('.attn_typ').css('display', 'flex');
			}else if (setting >2) {
				console.log(setting);
			 $('.attn_typ').css('display', 'flex');
			}
			else {
				$('.attn_typ').css('display', 'none');
			}
		})
		.error(function (data) {
		});
	// Get check_attn_mark_per_day END

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	var subjectID = localStorage.getItem("subjectID");

// subject for specfic teacher
	var subjectUrl=$scope.baseurl + "/api/staffAPI/specific_subject/";
	console.log("subjectUrl " + subjectUrl);
	$http.get(subjectUrl, { params: { "id": teacherID } })
	.success(function (data) {
		$(".dash_content").css('display', 'block');
		$(".dash_content_warning").html('');
	var status=data['status'];
if(status)
{
	$scope.specific_subject=data['data'];
	}
	})
	.error(function (data) {
		$(".dash_content_warning")
			.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
	});
	//periods
	var periodUrl=$scope.baseurl + "/api/staffAPI/get_timemaster";
	console.log("periodUrl " + periodUrl);
	$http.get(periodUrl, { params: { "key": "value" } })
	.success(function (data) {
		$(".dash_content").css('display', 'block');
		$(".dash_content_warning").html('');
		
	$scope.periods=data;
	})
	.error(function (data) {
		$(".dash_content_warning")
			.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
	});
	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1"} })
		.success(function (data) {
			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.mdSelectValue = data[0]["classesID"];
			$scope.datas_class = data;
			console.log($scope.datas_class);
			var CurrentTeacherurl = $scope.baseurl + "/api/staffAPI/get_currentteacherdetails/";
			console.log("CurrentTeacherurl " + CurrentTeacherurl);
		
			$http.get(CurrentTeacherurl, { params: { "teacherID": teacherID} })
				.success(function (data) {
		
					$(".dash_content").css('display', 'block');
					$(".dash_content_warning").html('');
					console.log('data');
					console.log(data);
					$scope.mdSelectValue = data[0]["classesID"];
					$scope.selectSpecific_subject=data[0]["subjectID"];
					$scope.selectPeriod=data[0]["period"];
					$scope.mdSelectValue1= data[0]["sectionID"];
					
					console.log($scope.mdSelectValue1);
					var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/";
		
				$http.get(url_sec, { params: { "teacherid": teacherID, "classid": $scope.mdSelectValue } })
					.success(function (data) {
						$scope.datasec = data;
					})
				})
				.error(function (data) {
					$(".dash_content_warning")
						.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
				});
		})
		.error(function (data) {
			$(".dash_content_warning")
				.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

		var selectSpecific_subject = localStorage.getItem("selectSpecific_subject");
		var selectPeriod = localStorage.getItem("selectPeriod");
		console.log("subject ID : "+selectSpecific_subject);
		console.log("subject ID : "+selectPeriod);

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;
		console.log(url_sec);
		$http.get(url_sec, { params: { "key1": "value1" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};
//after selecting subject
$scope.getSpecific_subject=function(specific_subject){
	console.log(specific_subject);
	localStorage.setItem("selectSpecific_subject", specific_subject);
	}
	//after selecting period
	$scope.getPeriod=function(periods,specific_subject){
	console.log(periods);
	console.log(specific_subject);
	localStorage.setItem("selectPeriod", periods);
	}
	$scope.session = function (mdSelectValue) {

		console.log(" Select Session  " + mdSelectValue);
		var result;
		if (mdSelectValue == 'Morning') {
			result = 1;
		} else if (mdSelectValue == 'Afternoon') {
			result = 2;
		}
		localStorage.setItem("selectedSession", mdSelectValue);
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		if ($scope.attendanceDay == '2') {
			$scope.selectedSession = localStorage.getItem("selectedSession");
			if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
				localStorage.setItem("selectedSession", 1);
			}
		} else {
			localStorage.setItem("selectedSession", 1);
		}
		$scope.selectedSession = localStorage.getItem("selectedSession");
		console.log("Check Session " + $scope.selectedSession);
		var session = $scope.selectedSession;
		var data = '';
		if ($scope.attendanceDay == '2') {
			data = session;
		}

		var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
		console.log(url);

		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);
				var output = data['output'];
				//if(output == 'Empty') { $scope.noti = '1'; }
				if (output == 0) {
					$scope.noti = '1';
					$('.attn_warn').html('');
					localStorage.setItem("editNotification", '0');
		
					$scope.editable = '0';
				}
				else {
					$scope.noti = '0';
					localStorage.setItem("editNotification", '1');
					$scope.editable = '1';
				}

				if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
				else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

				var attn_typ = localStorage.getItem("attn_typ");
				if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
				else {
					if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
					else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
				}
			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data not found...!!!');
			});
		// Get check_alreadymarked END

		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');

		$scope.morning = function () {
			//alert('morning');
			$('.attn_typ_btn_e').css('background-color', '#607d8b');
			$('.attn_typ_btn_m').css('background-color', '#445a10');
		};

		$scope.evening = function () {
			//alert('evening');
			$('.attn_typ_btn_m').css('background-color', '#607d8b');
			$('.attn_typ_btn_e').css('background-color', '#445a10');
		};


		// Get Student attendance list start
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var session = localStorage.getItem("selectedSession");
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		var data = '0';
		if ($scope.attendanceDay == '2') {
			data = session;
		}
		$scope.editable = localStorage.getItem("editNotification");
		console.log("Check Session " + $scope.editable);

		var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
		console.log(url);

		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);

				//$rootScope.name = data[0]['name'];
				//console.log(data[0]['name']);
				$scope.items = data;
				$scope.presents_length = $scope.items.length;
				$scope.absents = data[1];
				$scope.absents_length = $scope.items.length;
				//console.log(data.message);
				/*	if ($scope.items.length > 0) {
						$scope.noti = '1';
					}*/

					$get_value = [];

					for ($i = 0; $i < $scope.items.length; $i++) {
						var status = $scope.items[$i]['status'];
						if (status == 'A') {
							$get_val = $scope.items[$i];
							$get_value.push($get_val);
						}
					}
					$scope.selected1== $get_value;
					$scope.selected = data;
					$scope.selected2== $get_value;
console.log("Selected Length "+$scope.selected.length)

				$scope.presents = data;

				console.log(JSON.stringify($scope.presents));
				//$scope.selected =data[0];
				//$scope.page=1;
			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data Not Found...!!!');
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'alert-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: 'Data Not Found',
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$scope.noti = '0';
					//$state.reload(); 
					//$(".dash_content").css('display','none');
					//$window.location.reload(); 
				});
			});
		// Get Student attendance list end


	};
	/* Top Bar Code End */


	//if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
	//{

	//$state.go("app.selectclass");
	//}else{
	// For show Grid Bottom Sheet.
	$scope.showGridBottomSheet = function ($event) {
		$mdBottomSheet.show({
			templateUrl: 'ui-grid-bottom-sheet-template',
			targetEvent: $event,
			scope: $scope.$new(false),
		});
	};// End of showGridBottomSheet.

	$scope.doSecondaryAction = function (event) {
		$mdDialog.show(
			$mdDialog.alert()
				.title('Secondary Action')
				.textContent('Secondary actions can be used for one click actions')
				.ariaLabel('Secondary click demo')
				.ok('Neat!')
				.targetEvent(event)
		);
	};


	$scope.checkAll = function () {
		if ($scope.selectedAll) {
			$scope.selectedAll = true;
		} else {
			$scope.selectedAll = false;
		}
		angular.forEach($scope.person, function (item) {
			person.Selected = $scope.selectedAll;
		});
	};

	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");


	var staffID = localStorage.getItem("teacherID");
	//var staffID = '1';

	// Get Class name based on Class ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/classid/" + classID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.classname = data[0].classes;
			localStorage.setItem("classname", $scope.classname);
		})

	// Get Section name based on Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1" } })
		.success(function (data) {
			console.log(data);
			$scope.sectionname = data[0].section;
			localStorage.setItem("sectionname", $scope.sectionname);
		})

	// Get Class strength based on Class ID & Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstrengthdetails/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1"} })
		.success(function (data) {
			$scope.totalstrength = data[0].total;
			$scope.presentstrength = data[0].present;
			$scope.absentstrength = data[0].absent;
			//localStorage.setItem("classname", $scope.classname);
		})
		.error(function (data) {
			$scope.totalstrength = '-';
			$scope.presentstrength = '-';
			$scope.absentstrength = '-';
		})

	$scope.getCheckedFalse = function () {
		return false;
	};

	$scope.getCheckedTrue = function () {
		return true;
	};


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");

	$scope.attendanceDay = localStorage.getItem("attn_typ");
	if ($scope.attendanceDay == '2') {
		$scope.selectedSession = localStorage.getItem("selectedSession");
		if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
			localStorage.setItem("selectedSession", 1);
		}
	} else {
		localStorage.setItem("selectedSession", 1);
	}
	$scope.selectedSession = localStorage.getItem("selectedSession");
	console.log("Check Session " + $scope.selectedSession);
	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '1';
	if ($scope.attendanceDay == '2') {
		data = session;
	}

	var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var output = data['output'];
			//if(output == 'Empty') { $scope.noti = '1'; }
			if (output == 0) {
				$scope.noti = '1';
				$('.attn_warn').html('');
				localStorage.setItem("editNotification", '0');
				$scope.editable = '0';
			}
			else {
				$scope.noti = '0';
				localStorage.setItem("editNotification", '1');
				$scope.editable = '1';
			}
			if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
			else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

			var attn_typ = localStorage.getItem("attn_typ");
			if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
			else {
				if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
				else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
			}
		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data not found...!!!');
		});
	// Get check_alreadymarked END

	$('.attn_typ_btn_m').css('background-color', '#607d8b');
	$('.attn_typ_btn_e').css('background-color', '#607d8b');

	$scope.morning = function () {
		//alert('morning');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');
		$('.attn_typ_btn_m').css('background-color', '#445a10');
	};

	$scope.evening = function () {
		//alert('evening');
		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#445a10');
	};


	// Get Student attendance list start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.selectedSession = localStorage.getItem("selectedSession");

	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '0';
	if ($scope.attendanceDay == '2') {
		data = session;
	}
	$scope.editable = localStorage.getItem("editNotification");
	console.log("Check Session " + data);
	console.log("Check EDIT " + $scope.editable);
	var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;;
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);

			//$rootScope.name = data[0]['name'];
			//console.log(data[0]['name']);
			$scope.items = data;
			$scope.presents_length = $scope.items.length;
			$scope.absents = data[1];
			$scope.absents_length = $scope.items.length;
			//console.log(data.message);
			/*	if ($scope.items.length > 0) {
					$scope.noti = '1';
				}
				*/
			$get_value = [];

			for ($i = 0; $i < $scope.items.length; $i++) {
				var status = $scope.items[$i]['status'];
				if (status == 'A') {
					$get_val = $scope.items[$i];
					$get_value.push($get_val);
				}
			}
			$scope.selected1 =$get_value;
			$scope.selected =[];
			$scope.presents = data;
			$scope.selected2== $get_value;

			console.log(JSON.stringify($scope.presents));
			//$scope.selected =data[0];
			//$scope.page=1;
		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data Not Found...!!!');
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'alert-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: 'Data Not Found',
						content: "",
						ok: "OK"
					}
				}
			}).then(function () {
				$scope.noti = '0';
				//$state.reload(); 
				//$(".dash_content").css('display','none');
				//$window.location.reload(); 
			});
		});
	// Get Student attendance list end

	$scope.edit = function () {
		$scope.noti = '1';
		$scope.editable = '2';
		$('.attn_warn').html('');
	};

	//$scope.items = data[1];
	//$scope.items = [1,2,3,4,5];
	// $scope.selected = [];
	$scope.studentID = [];
	$scope.toggle = function (item, list) {
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	};

	$scope.exists1 = function (item, list) {

		return list.indexOf(item) > -1;
	};
	$scope.exists = function (item, list) {
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate = function () {
		return ($scope.selected.length !== 0 &&
			$scope.selected.length !== $scope.items.length);
	};

	$scope.isChecked = function () {
		if ($scope.items == null || $scope.items == "undefined" || $scope.items == '') {
			$scope.items = [];
		}
		return ($scope.selected.length === $scope.items.length);
	};

	$scope.toggleAll = function () {
		if ($scope.selected.length === $scope.items.length) {
			$scope.selected = [];
			$scope.studentID = [];

		} else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
			$scope.selected = $scope.items.slice(0);
			$scope.isDisabled = false;
			//$scope.datas = angular.copy($scope.items).slice(0, 3);
			//$scope.studentID = $scope.datas[-1]['studentID'];
		}
	};

	$scope.mark_attendance = function () {

		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'confirm-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: "Alert",
					content: "Do you want to submit attendance?",
					ok: "YES DO IT",
					cancel: "Cancel"
				}
			}
		}).then(function () {
			$scope.isDisabled = true;
			$arr_val = $scope.selected;
			$arr_length = $arr_val.length;
			console.log($scope.selected);
			console.log($arr_length);
			var sdata_obj = JSON.stringify($scope.selected);
			console.log(sdata_obj);
			$get_value = [];
			for ($i = 0; $i < $arr_length; $i++) {
				$get_val = $arr_val[$i]['studentID'];
				$get_value.push($get_val);
			}
			console.log($get_value);
			$scope.prvalue = $get_value;

			$arr_val1 = $scope.selected1;
			$arr_length1 = $arr_val1.length;
			console.log($scope.selected1);
			console.log($arr_length1);
			var sdata_obj1 = JSON.stringify($scope.selected1);
			console.log(sdata_obj1);
			$get_value1 = [];
			for ($i = 0; $i < $arr_length1; $i++) {
				$get_val1 = $arr_val1[$i]['studentID'];
				$get_value1.push($get_val1);
			}
			console.log($get_value1);
			$scope.abvalue = $get_value1;

			$c = $scope.prvalue.concat($scope.abvalue);
			console.log($c);

			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			var url = $scope.baseurl + "/api/staffAPI/attendance_register/";
			var datas = $c;
			console.log(datas);

			$scope.session = localStorage.getItem("selectedSession");
			var staffID = localStorage.getItem("teacherID");
			var str_obj = JSON.stringify(datas);

			console.log(str_obj);
			console.log($scope.session);
			$http.get(url, { params: { "data": str_obj, "classID": classID, "sectionID": sectionID, "staffID": staffID, "session": $scope.session,
		"subjectID": localStorage.getItem("selectSpecific_subject"),"period": localStorage.getItem("selectPeriod") } })
				.success(function (data) {
					console.log(data);

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Registered',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});

				})
				.error(function (data) {

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Failed',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});
				});
		});
	};

	$scope.selected1 = [];
	$scope.studentID1 = [];
	$scope.toggle1 = function (item, list) {
		var idx = list.indexOf(item);
		if(item.status == 'A')
		{
		if (idx > -1) {
			item.status = 'P';
			list.splice(idx, 1);
		}
		else {
			item.status = 'A';
			list.push(item);
		}
	}else{
		if (idx > -1) {
			item.status = 'P';
			list.splice(idx, 1);
		}
		else {
			item.status = 'A';
			list.push(item);
		}
	}
	};

	$scope.exists1 = function (item, list) {
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate1 = function () {
		return ($scope.selected1.length !== 0 &&
			$scope.selected1.length !== $scope.presents.length);
	};

	$scope.isChecked1 = function () {
		//console.log($scope.selected1);
		if ($scope.presents == null || $scope.presents == "undefined" || $scope.presents == '') {
			$scope.presents = [];
		}
		//console.log($scope.presents.length);
		return $scope.selected1.length === $scope.presents.length;
	};

	$scope.toggleAll1 = function () {
		$scope.isDisabled = false;
		if ($scope.selected1.length === $scope.presents.length) {
			$scope.selected1 = [];
			$scope.studentID1 = [];
			for (var i = 0; i < $scope.presents.length; i++) {
				var item = $scope.presents[i];
				item.status = 'P';
			}

		} else if ($scope.selected1.length === 0 || $scope.selected1.length > 0) {
			$scope.selected1 = $scope.presents.slice(0);
			var sdata = $scope.presents.slice(0);
			for (var i = 0; i < $scope.presents.length; i++) {
				var item = $scope.presents[i];
				item.status = 'A';
			}
		}
	};

	//}
}); // End of  mark attendance controller.

// Controller of mark attendance page.
appControllers.controller('mark_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http) {

	$scope.reloadPage = function () { $state.reload(); }
	$scope.date = new Date();
	localStorage.setItem("selectedSession", null);
	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");
	$scope.isDisabled = true;
	$('.attn_warn').html('');
	// Get check_attn_mark_per_day start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/attn_mark_per_day";
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var setting = data[0]['setting'];
			$scope.attendanceperDay = setting;
			console.log(setting);
			localStorage.setItem("attn_typ", setting);
			if (setting == '2') {
				$('.attn_typ').css('display', 'flex');
			}else if (setting >2) {
				console.log(setting);
				$state.go("app.hourly_attendance");
			}
			else {
				$('.attn_typ').css('display', 'none');
			}
		})
		.error(function (data) {
		});
	// Get check_attn_mark_per_day END

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning")
				.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/";

		$http.get(url_sec, { params: { "teacherid": teacherID, "classid": classID} })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/";

		$http.get(url_sec, { params: {"teacherid": teacherID, "classid": chrf } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};


	$scope.session = function (mdSelectValue) {

		console.log(" Select Session  " + mdSelectValue);
		var result;
		if (mdSelectValue == 'Morning') {
			result = 1;
		} else if (mdSelectValue == 'Afternoon') {
			result = 2;
		}
		localStorage.setItem("selectedSession", mdSelectValue);
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		if ($scope.attendanceDay == '2') {
			$scope.selectedSession = localStorage.getItem("selectedSession");
			if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
				localStorage.setItem("selectedSession", 1);
			}
		} else {
			localStorage.setItem("selectedSession", 1);
		}
		$scope.selectedSession = localStorage.getItem("selectedSession");
		console.log("Check Session " + $scope.selectedSession);
		var session = $scope.selectedSession;
		var data = '';
		if ($scope.attendanceDay == '2') {
			data = session;
		}

		var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
		console.log(url);

		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);
				var output = data['output'];
				//if(output == 'Empty') { $scope.noti = '1'; }
				if (output == 0) {
					$scope.noti = '1';
					$('.attn_warn').html('');
					localStorage.setItem("editNotification", '0');
		
					$scope.editable = '0';
				}
				else {
					$scope.noti = '0';
					localStorage.setItem("editNotification", '1');
					$scope.editable = '1';
				}

				if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
				else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

				var attn_typ = localStorage.getItem("attn_typ");
				if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
				else {
					if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
					else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
				}
			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data not found...!!!');
			});
		// Get check_alreadymarked END

		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');

		$scope.morning = function () {
			//alert('morning');
			$('.attn_typ_btn_e').css('background-color', '#607d8b');
			$('.attn_typ_btn_m').css('background-color', '#445a10');
		};

		$scope.evening = function () {
			//alert('evening');
			$('.attn_typ_btn_m').css('background-color', '#607d8b');
			$('.attn_typ_btn_e').css('background-color', '#445a10');
		};


		// Get Student attendance list start
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var session = localStorage.getItem("selectedSession");
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		var data = '0';
		if ($scope.attendanceDay == '2') {
			data = session;
		}
		$scope.editable = localStorage.getItem("editNotification");
		console.log("Check Session " + $scope.editable);

		var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
		console.log(url);

		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);

				//$rootScope.name = data[0]['name'];
				//console.log(data[0]['name']);
				$scope.items = data;
				$scope.presents_length = $scope.items.length;
				$scope.absents = data[1];
				$scope.absents_length = $scope.items.length;
				//console.log(data.message);
				/*	if ($scope.items.length > 0) {
						$scope.noti = '1';
					}*/

					$get_value = [];

					for ($i = 0; $i < $scope.items.length; $i++) {
						var status = $scope.items[$i]['status'];
						if (status == 'A') {
							$get_val = $scope.items[$i];
							$get_value.push($get_val);
						}
					}
					$scope.selected1== $get_value;
					$scope.selected = data;
					$scope.selected2== $get_value;
console.log("Selected Length "+$scope.selected.length)

				$scope.presents = data;

				console.log(JSON.stringify($scope.presents));
				//$scope.selected =data[0];
				//$scope.page=1;
			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data Not Found...!!!');
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'alert-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: 'Data Not Found',
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$scope.noti = '0';
					//$state.reload(); 
					//$(".dash_content").css('display','none');
					//$window.location.reload(); 
				});
			});
		// Get Student attendance list end


	};
	/* Top Bar Code End */


	//if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
	//{

	//$state.go("app.selectclass");
	//}else{
	// For show Grid Bottom Sheet.
	$scope.showGridBottomSheet = function ($event) {
		$mdBottomSheet.show({
			templateUrl: 'ui-grid-bottom-sheet-template',
			targetEvent: $event,
			scope: $scope.$new(false),
		});
	};// End of showGridBottomSheet.

	$scope.doSecondaryAction = function (event) {
		$mdDialog.show(
			$mdDialog.alert()
				.title('Secondary Action')
				.textContent('Secondary actions can be used for one click actions')
				.ariaLabel('Secondary click demo')
				.ok('Neat!')
				.targetEvent(event)
		);
	};


	$scope.checkAll = function () {
		if ($scope.selectedAll) {
			$scope.selectedAll = true;
		} else {
			$scope.selectedAll = false;
		}
		angular.forEach($scope.person, function (item) {
			person.Selected = $scope.selectedAll;
		});
	};

	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");


	var staffID = localStorage.getItem("teacherID");
	//var staffID = '1';

	// Get Class name based on Class ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/classid/" + classID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.classname = data[0].classes;
			localStorage.setItem("classname", $scope.classname);
		})

	// Get Section name based on Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsectionlist//api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1" } })
		.success(function (data) {
			console.log(data);
			$scope.sectionname = data[0].section;
			localStorage.setItem("sectionname", $scope.sectionname);
		})

	// Get Class strength based on Class ID & Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstrengthdetails/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1"} })
		.success(function (data) {
			$scope.totalstrength = data[0].total;
			$scope.presentstrength = data[0].present;
			$scope.absentstrength = data[0].absent;
			//localStorage.setItem("classname", $scope.classname);
		})
		.error(function (data) {
			$scope.totalstrength = '-';
			$scope.presentstrength = '-';
			$scope.absentstrength = '-';
		})

	$scope.getCheckedFalse = function () {
		return false;
	};

	$scope.getCheckedTrue = function () {
		return true;
	};


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");

	$scope.attendanceDay = localStorage.getItem("attn_typ");
	if ($scope.attendanceDay == '2') {
		$scope.selectedSession = localStorage.getItem("selectedSession");
		if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
			localStorage.setItem("selectedSession", 1);
		}
	} else {
		localStorage.setItem("selectedSession", 1);
	}
	$scope.selectedSession = localStorage.getItem("selectedSession");
	console.log("Check Session " + $scope.selectedSession);
	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '1';
	if ($scope.attendanceDay == '2') {
		data = session;
	}

	var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var output = data['output'];
			//if(output == 'Empty') { $scope.noti = '1'; }
			if (output == 0) {
				$scope.noti = '1';
				$('.attn_warn').html('');
				localStorage.setItem("editNotification", '0');
				$scope.editable = '0';
			}
			else {
				$scope.noti = '0';
				localStorage.setItem("editNotification", '1');
				$scope.editable = '1';
			}
			if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
			else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

			var attn_typ = localStorage.getItem("attn_typ");
			if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
			else {
				if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
				else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
			}
		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data not found...!!!');
		});
	// Get check_alreadymarked END

	$('.attn_typ_btn_m').css('background-color', '#607d8b');
	$('.attn_typ_btn_e').css('background-color', '#607d8b');

	$scope.morning = function () {
		//alert('morning');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');
		$('.attn_typ_btn_m').css('background-color', '#445a10');
	};

	$scope.evening = function () {
		//alert('evening');
		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#445a10');
	};


	// Get Student attendance list start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.selectedSession = localStorage.getItem("selectedSession");

	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '0';
	if ($scope.attendanceDay == '2') {
		data = session;
	}
	$scope.editable = localStorage.getItem("editNotification");
	console.log("Check Session " + data);
	console.log("Check EDIT " + $scope.editable);
	var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;;
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);

			//$rootScope.name = data[0]['name'];
			//console.log(data[0]['name']);
			$scope.items = data;
			$scope.presents_length = $scope.items.length;
			$scope.absents = data[1];
			$scope.absents_length = $scope.items.length;
			//console.log(data.message);
			/*	if ($scope.items.length > 0) {
					$scope.noti = '1';
				}
				*/
			$get_value = [];

			for ($i = 0; $i < $scope.items.length; $i++) {
				var status = $scope.items[$i]['status'];
				if (status == 'A') {
					$get_val = $scope.items[$i];
					$get_value.push($get_val);
				}
			}
			$scope.selected1 =$get_value;
			$scope.selected =[];
			$scope.presents = data;
			$scope.selected2== $get_value;

			console.log(JSON.stringify($scope.presents));
			//$scope.selected =data[0];
			//$scope.page=1;
		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data Not Found...!!!');
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'alert-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: 'Data Not Found',
						content: "",
						ok: "OK"
					}
				}
			}).then(function () {
				$scope.noti = '0';
				//$state.reload(); 
				//$(".dash_content").css('display','none');
				//$window.location.reload(); 
			});
		});
	// Get Student attendance list end

	$scope.edit = function () {
		$scope.noti = '1';
		$scope.editable = '2';
		$('.attn_warn').html('');
	};

	//$scope.items = data[1];
	//$scope.items = [1,2,3,4,5];
	// $scope.selected = [];
	$scope.studentID = [];
	$scope.toggle = function (item, list) {
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	};

	$scope.exists1 = function (item, list) {

		return list.indexOf(item) > -1;
	};
	$scope.exists = function (item, list) {
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate = function () {
		return ($scope.selected.length !== 0 &&
			$scope.selected.length !== $scope.items.length);
	};

	$scope.isChecked = function () {
		if ($scope.items == null || $scope.items == "undefined" || $scope.items == '') {
			$scope.items = [];
		}
		return ($scope.selected.length === $scope.items.length);
	};

	$scope.toggleAll = function () {
		if ($scope.selected.length === $scope.items.length) {
			$scope.selected = [];
			$scope.studentID = [];

		} else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
			$scope.selected = $scope.items.slice(0);
			$scope.isDisabled = false;
			//$scope.datas = angular.copy($scope.items).slice(0, 3);
			//$scope.studentID = $scope.datas[-1]['studentID'];
		}
	};

	$scope.mark_attendance = function () {

		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'confirm-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: "Alert",
					content: "Do you want to submit attendance?",
					ok: "YES DO IT",
					cancel: "Cancel"
				}
			}
		}).then(function () {
			$scope.isDisabled = true;
			$arr_val = $scope.selected;
			$arr_length = $arr_val.length;
			console.log($scope.selected);
			console.log($arr_length);
			var sdata_obj = JSON.stringify($scope.selected);
			console.log(sdata_obj);
			$get_value = [];
			for ($i = 0; $i < $arr_length; $i++) {
				$get_val = $arr_val[$i]['studentID'];
				$get_value.push($get_val);
			}
			console.log($get_value);
			$scope.prvalue = $get_value;

			$arr_val1 = $scope.selected1;
			$arr_length1 = $arr_val1.length;
			console.log($scope.selected1);
			console.log($arr_length1);
			var sdata_obj1 = JSON.stringify($scope.selected1);
			console.log(sdata_obj1);
			$get_value1 = [];
			for ($i = 0; $i < $arr_length1; $i++) {
				$get_val1 = $arr_val1[$i]['studentID'];
				$get_value1.push($get_val1);
			}
			console.log($get_value1);
			$scope.abvalue = $get_value1;

			$c = $scope.prvalue.concat($scope.abvalue);
			console.log($c);

			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			var url = $scope.baseurl + "/api/staffAPI/attendance_register/";
			var datas = $c;
			console.log(datas);

			$scope.session = localStorage.getItem("selectedSession");
			var staffID = localStorage.getItem("teacherID");
			var str_obj = JSON.stringify(datas);

			console.log(str_obj);
			console.log($scope.session);
			$http.get(url, { params: { "data": str_obj, "classID": classID, "sectionID": sectionID, "staffID": staffID, "session": $scope.session } })
				.success(function (data) {
					console.log(data);

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Registered',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});

				})
				.error(function (data) {

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Failed',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});
				});
		});
	};

	$scope.selected1 = [];
	$scope.studentID1 = [];
	$scope.toggle1 = function (item, list) {
		var idx = list.indexOf(item);
		if(item.status == 'A')
		{
		if (idx > -1) {
			item.status = 'P';
			list.splice(idx, 1);
		}
		else {
			item.status = 'A';
			list.push(item);
		}
	}else{
		if (idx > -1) {
			item.status = 'P';
			list.splice(idx, 1);
		}
		else {
			item.status = 'A';
			list.push(item);
		}
	}
	};

	$scope.exists1 = function (item, list) {
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate1 = function () {
		return ($scope.selected1.length !== 0 &&
			$scope.selected1.length !== $scope.presents.length);
	};

	$scope.isChecked1 = function () {
		//console.log($scope.selected1);
		if ($scope.presents == null || $scope.presents == "undefined" || $scope.presents == '') {
			$scope.presents = [];
		}
		//console.log($scope.presents.length);
		return $scope.selected1.length === $scope.presents.length;
	};

	$scope.toggleAll1 = function () {
		$scope.isDisabled = false;
		if ($scope.selected1.length === $scope.presents.length) {
			$scope.selected1 = [];
			$scope.studentID1 = [];
			for (var i = 0; i < $scope.presents.length; i++) {
				var item = $scope.presents[i];
				item.status = 'P';
			}

		} else if ($scope.selected1.length === 0 || $scope.selected1.length > 0) {
			$scope.selected1 = $scope.presents.slice(0);
			var sdata = $scope.presents.slice(0);
			for (var i = 0; i < $scope.presents.length; i++) {
				var item = $scope.presents[i];
				item.status = 'A';
			}
		}
	};

	//}
}); // End of  mark attendance controller.

// Controller of mark attendance page.
appControllers.controller('automaticCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http) {

	$scope.reloadPage = function () { $state.reload(); }
	$scope.date = new Date();
	localStorage.setItem("selectedSession", null);
	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");
	$scope.isDisabled = true;
	$('.attn_warn').html('');
	// Get check_attn_mark_per_day start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/attn_mark_per_day";
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var setting = data[0]['setting'];
			$scope.attendanceperDay = setting;
			localStorage.setItem("attn_typ", setting);
			if (setting == '2') {
				$('.attn_typ').css('display', 'flex');
			}else if (setting >2) {
				console.log(setting);
				$state.go("app.hourly_attendance");
			}
			else {
				$('.attn_typ').css('display', 'none');
			}
		})
		.error(function (data) {
		});
	// Get check_attn_mark_per_day END

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning")
				.html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist//api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist//api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};


	$scope.session = function (mdSelectValue) {

		console.log(" Select Session  " + mdSelectValue);
		var result;
		if (mdSelectValue == 'Morning') {
			result = 1;
		} else if (mdSelectValue == 'Afternoon') {
			result = 2;
		}
		localStorage.setItem("selectedSession", mdSelectValue);
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		if ($scope.attendanceDay == '2') {
			$scope.selectedSession = localStorage.getItem("selectedSession");
			if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
				localStorage.setItem("selectedSession", 1);
			}
		} else {
			localStorage.setItem("selectedSession", 1);
		}
		$scope.selectedSession = localStorage.getItem("selectedSession");
		console.log("Check Session " + $scope.selectedSession);
		var session = $scope.selectedSession;
		var data = '';
		if ($scope.attendanceDay == '2') {
			data = session;
		}

		var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
		console.log(url);

		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);
				var output = data['output'];
				//if(output == 'Empty') { $scope.noti = '1'; }
				if (output == 0) {
					$scope.noti = '1';
					$('.attn_warn').html('');
				}
				else {
					$scope.noti = '0';
					localStorage.setItem("editNotification", '1');
				}

				if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
				else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

				var attn_typ = localStorage.getItem("attn_typ");
				if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
				else {
					if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
					else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
				}
			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data not found...!!!');
			});
		// Get check_alreadymarked END

		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');

		$scope.morning = function () {
			//alert('morning');
			$('.attn_typ_btn_e').css('background-color', '#607d8b');
			$('.attn_typ_btn_m').css('background-color', '#445a10');
		};

		$scope.evening = function () {
			//alert('evening');
			$('.attn_typ_btn_m').css('background-color', '#607d8b');
			$('.attn_typ_btn_e').css('background-color', '#445a10');
		};


		// Get Student attendance list start
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var session = localStorage.getItem("selectedSession");
		$scope.attendanceDay = localStorage.getItem("attn_typ");
		var data = '0';
		if ($scope.attendanceDay == '2') {
			data = session;
		}
		$scope.editable = localStorage.getItem("editNotification");
		console.log("Check Edit  " + $scope.editable);
		//var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;;
		//	console.log(url);
		var url = $scope.baseurl + "/api/staffAPI/getreceiverdata/class/" + classID + "/section/" + sectionID;
		console.log(url);
		$http.get(url, { params: {} })
			.success(function (data) {
				console.log(data);
				console.log(data);
				$scope.presents = data[0];
				$scope.selected = [];
				$scope.presents_length = $scope.presents.length;
				$scope.items = data[1];
				console.log(JSON.stringify($scope.items));
				console.log(JSON.stringify($scope.presents));

			})
			.error(function (data) {
				$scope.editable = '0';
				localStorage.setItem("editNotification", '0');
				$('.attn_warn').html('Data Not Found...!!!');
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'alert-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: 'Data Not Found',
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$scope.noti = '0';

				});
			});
		// Get Student attendance list end


	};
	/* Top Bar Code End */


	//if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
	//{

	//$state.go("app.selectclass");
	//}else{
	// For show Grid Bottom Sheet.
	$scope.showGridBottomSheet = function ($event) {
		$mdBottomSheet.show({
			templateUrl: 'ui-grid-bottom-sheet-template',
			targetEvent: $event,
			scope: $scope.$new(false),
		});
	};// End of showGridBottomSheet.

	$scope.doSecondaryAction = function (event) {
		$mdDialog.show(
			$mdDialog.alert()
				.title('Secondary Action')
				.textContent('Secondary actions can be used for one click actions')
				.ariaLabel('Secondary click demo')
				.ok('Neat!')
				.targetEvent(event)
		);
	};


	$scope.checkAll = function () {
		if ($scope.selectedAll) {
			$scope.selectedAll = true;
		} else {
			$scope.selectedAll = false;
		}
		angular.forEach($scope.person, function (item) {
			person.Selected = $scope.selectedAll;
		});
	};

	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");


	var staffID = localStorage.getItem("teacherID");
	//var staffID = '1';

	// Get Class name based on Class ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/classid/" + classID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.classname = data[0].classes;
			localStorage.setItem("classname", $scope.classname);
		})

	// Get Section name based on Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsectionlist//api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.sectionname = data[0].section;
			localStorage.setItem("sectionname", $scope.sectionname);
		})

	// Get Class strength based on Class ID & Section ID
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstrengthdetails/classid/" + classID + "/sectionid/" + sectionID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			$scope.totalstrength = data[0].total;
			$scope.presentstrength = data[0].present;
			$scope.absentstrength = data[0].absent;
			//localStorage.setItem("classname", $scope.classname);
		})
		.error(function (data) {
			$scope.totalstrength = '-';
			$scope.presentstrength = '-';
			$scope.absentstrength = '-';
		})

	$scope.getCheckedFalse = function () {
		return false;
	};

	$scope.getCheckedTrue = function () {
		return true;
	};


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");

	$scope.attendanceDay = localStorage.getItem("attn_typ");
	if ($scope.attendanceDay == '2') {
		$scope.selectedSession = localStorage.getItem("selectedSession");
		if ($scope.selectedSession == null || $scope.selectedSession == 'null' || $scope.selectedSession.length == 0) {
			localStorage.setItem("selectedSession", 1);
		}
	} else {
		localStorage.setItem("selectedSession", 1);
	}
	$scope.selectedSession = localStorage.getItem("selectedSession");
	console.log("Check Session " + $scope.selectedSession);
	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '1';
	if ($scope.attendanceDay == '2') {
		data = session;
	}

	var url = $scope.baseurl + "/api/staffAPI/check_alreadymarked/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var output = data['output'];
			//if(output == 'Empty') { $scope.noti = '1'; }
			if (output == 0) {
				$scope.noti = '1';
				$('.attn_warn').html('');
			}
			else {
				$scope.noti = '0';
				localStorage.setItem("editNotification", '1');
			}
			if (session == '1') { $('.attn_typ_btn_m').css('background-color', '#445a10'); }
			else if (session == '2') { $('.attn_typ_btn_e').css('background-color', '#445a10'); }

			var attn_typ = localStorage.getItem("attn_typ");
			if ((attn_typ == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Attendance marked today...!!!'); }
			else {
				if ((session == '1') && ($scope.noti == '0')) { $('.attn_warn').html('Morning attendance marked today...!!!'); }
				else if ((session == '2') && ($scope.noti == '0')) { $('.attn_warn').html('Afternoon attendance marked today...!!!'); }
			}
		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data not found...!!!');
		});
	// Get check_alreadymarked END

	$('.attn_typ_btn_m').css('background-color', '#607d8b');
	$('.attn_typ_btn_e').css('background-color', '#607d8b');

	$scope.morning = function () {
		//alert('morning');
		$('.attn_typ_btn_e').css('background-color', '#607d8b');
		$('.attn_typ_btn_m').css('background-color', '#445a10');
	};

	$scope.evening = function () {
		//alert('evening');
		$('.attn_typ_btn_m').css('background-color', '#607d8b');
		$('.attn_typ_btn_e').css('background-color', '#445a10');
	};


	// Get Student attendance list start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.selectedSession = localStorage.getItem("selectedSession");

	var session = localStorage.getItem("selectedSession");
	$scope.attendanceDay = localStorage.getItem("attn_typ");
	var data = '0';
	if ($scope.attendanceDay == '2') {
		data = session;
	}
	$scope.editable = localStorage.getItem("editNotification");
	console.log("Check Session " + data);
	console.log("Check EDIT " + $scope.editable);
	//var url = $scope.baseurl + "/api/staffAPI/getstudentlist/classid/" + classID + "/sectionid/" + sectionID + "/session/" + data;;
	//	console.log(url);
	var url = $scope.baseurl + "/api/staffAPI/getreceiverdata/class/" + classID + "/section/" + sectionID;
	console.log(url);
	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			console.log(data);
			$scope.presents = data[0];
			$scope.selected = [];
			$scope.presents_length = $scope.presents.length;
			$scope.items = data[1];
			console.log(JSON.stringify($scope.items));
			console.log(JSON.stringify($scope.presents));

		})
		.error(function (data) {
			$scope.editable = '0';
			localStorage.setItem("editNotification", '0');
			$('.attn_warn').html('Data Not Found...!!!');
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'alert-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: 'Data Not Found',
						content: "",
						ok: "OK"
					}
				}
			}).then(function () {
				$scope.noti = '0';

			});
		});
	// Get Student attendance list end

	$scope.edit = function () {
		$scope.noti = '1';
		$scope.editable = '0';
		$('.attn_warn').html('');
	};

	//$scope.items = data[1];
	//$scope.items = [1,2,3,4,5];
	// $scope.selected = [];
	$scope.studentID = [];
	$scope.toggle = function (item, list) {
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	};


	$scope.exists1 = function (item, list) {
		console.log("ITMES " + item)

		console.log("JSON " + JSON.stringify(item))
		return list.indexOf(item) > -1;
	};
	$scope.exists = function (item, list) {
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate = function () {
		return ($scope.selected.length !== 0 &&
			$scope.selected.length !== $scope.items.length);
	};

	$scope.isChecked = function () {
		if ($scope.items == null || $scope.items == "undefined" || $scope.items == '') {
			$scope.items = [];
		}
		return ($scope.selected.length === $scope.items.length);
	};

	$scope.toggleAll = function () {
		if ($scope.selected.length === $scope.items.length) {
			$scope.selected = [];
			$scope.studentID = [];
		} else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
			$scope.selected = $scope.items.slice(0);
			$scope.isDisabled = false;
			//$scope.datas = angular.copy($scope.items).slice(0, 3);
			//$scope.studentID = $scope.datas[-1]['studentID'];
		}
	};

	$scope.mark_attendance = function () {

		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'confirm-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: "Alert",
					content: "Do you want to submit attendance?",
					ok: "YES DO IT",
					cancel: "Cancel"
				}
			}
		}).then(function () {
			$scope.isDisabled = true;
			$arr_val = $scope.selected;
			$arr_length = $arr_val.length;
			console.log($scope.selected);
			console.log($arr_length);
			var sdata_obj = JSON.stringify($scope.selected);
			console.log(sdata_obj);
			$get_value = [];
			for ($i = 0; $i < $arr_length; $i++) {
				$get_val = $arr_val[$i]['studentID'];
				$get_value.push($get_val);
			}
			console.log($get_value);
			$scope.prvalue = $get_value;

			$arr_val1 = $scope.selected1;
			$arr_length1 = $arr_val1.length;
			console.log($scope.selected1);
			console.log($arr_length1);
			var sdata_obj1 = JSON.stringify($scope.selected1);
			console.log(sdata_obj1);
			$get_value1 = [];
			for ($i = 0; $i < $arr_length1; $i++) {
				$get_val1 = $arr_val1[$i]['studentID'];
				$get_value1.push($get_val1);
			}
			console.log($get_value1);
			$scope.abvalue = $get_value1;

			$c = $scope.prvalue.concat($scope.abvalue);
			console.log($c);

			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			var url = $scope.baseurl + "/api/staffAPI/attendance_register/";
			var datas = $c;
			console.log(datas);

			$scope.session = localStorage.getItem("selectedSession");
			var staffID = localStorage.getItem("teacherID");
			var str_obj = JSON.stringify(datas);

			console.log(str_obj);
			console.log($scope.session);
			$http.get(url, { params: { "data": str_obj, "classID": classID, "sectionID": sectionID, "staffID": staffID, "session": $scope.session } })
				.success(function (data) {
					console.log(data);

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Registered',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});

				})
				.error(function (data) {

					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Failed',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();

					});
				});


		});
	};

	$scope.selected1 = [];
	$scope.studentID1 = [];
	$scope.toggle1 = function (item, list) {
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	};

	$scope.exists1 = function (item, list) {
		console.log("ITMES " + item)
		console.log("JSON " + JSON.stringify(item))
		return list.indexOf(item) > -1;
	};

	$scope.isIndeterminate1 = function () {
		return ($scope.selected1.length !== 0 &&
			$scope.selected1.length !== $scope.presents.length);
	};

	$scope.isChecked1 = function () {
		//console.log($scope.selected1);
		if ($scope.presents == null || $scope.presents == "undefined" || $scope.presents == '') {
			$scope.presents = [];
		}
		//console.log($scope.presents.length);
		return $scope.selected1.length === $scope.presents.length;
	};

	$scope.toggleAll1 = function () {
		$scope.isDisabled = false;
		if ($scope.selected1.length === $scope.presents.length) {
			$scope.selected1 = [];
			$scope.studentID1 = [];
			for (var i = 0; i < $scope.presents.length; i++) {
				var item = $scope.presents[i];
				item.status = 'A';

			}
		} else if ($scope.selected1.length === 0 || $scope.selected1.length > 0) {
			$scope.selected1 = $scope.presents.slice(0);
			var sdata = $scope.presents.slice(0);
		}
	};
	//}
}); // End of  mark attendance controller.







// Controller of view attendance page.
appControllers.controller('view_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http, $filter) {


	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsession";

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			var session = data['session'];
			localStorage.setItem("session", session);
			$scope.data = {
				group1: session
			};
		})
		.error(function (data) {
		});
	// Get session end


	$scope.init = '1';
	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;



		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};
	/* Top Bar Code End */


	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");
	/*	$scope.classID = '3';
		$scope.sectionID = '11';
		$scope.classname = '3';
		$scope.sectionname = 'A';*/
	$scope.reloadPage = function () { $state.reload(); }

	//if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
	//{

	//$state.go("app.selectclass");
	//}else{
	//$scope.defaultData = '';

	//$scope.currentdate = $filter("date")(Date.now(), 'dd-mmm-yyyy');

	var date = new Date(),
		mnth = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);
	$scope.date = [date.getFullYear(), mnth, day].join("-");
	//	$scope.date= '2016-12-28';
	console.log($scope.date);
	$scope.s_date = [date.getFullYear(), mnth, day].join("-");
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.session = localStorage.getItem("session");
	var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + $scope.session + "/date/" + $scope.date + "?format=json";
	console.log(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			//alert("SUCCESS");
			console.log(data);
			$scope.defaultData = data;
			//$scope.s_date = $scope.date;
			//alert($scope.s_date);
			//$scope.$apply();
			//window.localStorage.setItem('studata', data);
		})
		.error(function (data) {
			$scope.defaultData = '';


			//$scope.radioData = '';
			//window.localStorage.setItem('studata', '');
		});

	$scope.classattend = function (date) {
		console.log(date);
		var s_date = new Date(date),
			mnth = ("0" + (date.getMonth() + 1)).slice(-2),
			day = ("0" + date.getDate()).slice(-2);
		$scope.s_date = [date.getFullYear(), mnth, day].join("-");
		console.log($scope.date);
		$scope.s_date = $scope.s_date;
		$scope.init = '0';
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		$scope.session = localStorage.getItem("session");
		var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + $scope.session + "/date/" + $scope.s_date + "?format=json";
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.defaultData = data;

				//	$scope.$apply();
				//window.localStorage.setItem('studata', data);
			})
			.error(function (data) {
				$scope.defaultData = '';
				//$scope.radioData = '';
				//window.localStorage.setItem('studata', '');
			});
	};

	$scope.studentsearch = function (studentID) {
		$scope.studentID = studentID;
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/getsingleattendance/studentid/" + $scope.studentID + "?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.studentname = data[0]['name'];
				$scope.classesname = data[0]['classname'] + '-' + data[0]['sectionname'];
				$scope.percent = data[0]['percentage'];
				$scope.presentdays = data[0]['present'];
				$scope.absentdays = data[0]['absent'];
				$scope.workingdays = data[0]['total_working_days'];
				//window.localStorage.setItem('studata', data);
			})
			.error(function (data) {
				$scope.studentname = '';
				$scope.classesname = '';
				$scope.percent = '';
				$scope.presentdays = '';
				$scope.workingdays = '';
				$scope.absentdays = '';
				//$scope.radioData = '';
				//window.localStorage.setItem('studata', '');
			});
	};

	// Get check_attn_mark_per_day start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/attn_mark_per_day";
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var setting = data[0]['setting'];
			if (setting == '2') { $('.attn_typ').css('display', 'block'); }
			else { $('.attn_typ').css('display', 'none'); }
		})
		.error(function (data) {
		});
	// Get check_attn_mark_per_day END


	$scope.radioChanged = function () {
		var session = $scope.data.group1;
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + session + "/date/" + $scope.s_date + "?format=json";
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.defaultData = data;
			})
			.error(function (data) {
				$scope.defaultData = '';
			});
	};

	// For show Grid Bottom Sheet.
	$scope.showGridBottomSheet = function ($event) {
		$mdBottomSheet.show({
			templateUrl: 'ui-grid-bottom-sheet-template',
			targetEvent: $event,
			scope: $scope.$new(false),
		});
	};// End of showGridBottomSheet.


	$scope.doSecondaryAction = function (event) {
		$mdDialog.show(
			$mdDialog.alert()
				.title('Secondary Action')
				.textContent('Secondary actions can be used for one click actions')
				.ariaLabel('Secondary click demo')
				.ok('Neat!')
				.targetEvent(event)
		);
	};


	$scope.checkAll = function () {
		if ($scope.selectedAll) {
			$scope.selectedAll = true;
		} else {
			$scope.selectedAll = false;
		}
		angular.forEach($scope.person, function (item) {
			person.Selected = $scope.selectedAll;
		});

	};
	//}
}); // End of  view attendance controller.



// Controller of edit attendance page.
appControllers.controller('edit_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http, $filter) {

	// Get session start
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getsession";

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			var session = data['session'];
			localStorage.setItem("session", session);
			$scope.data = {
				group1: session
			};
		})
		.error(function (data) {
		});
	// Get session end

	$scope.init = '1';
	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;



		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};
	/* Top Bar Code End */


	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");

	$scope.reloadPage = function () { $state.reload(); }



	var date = new Date(),
		mnth = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);
	$scope.date = [date.getFullYear(), mnth, day].join("-");
	//	$scope.date= '2016-12-28';
	console.log($scope.date);
	$scope.s_date = [date.getFullYear(), mnth, day].join("-");
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	$scope.session = localStorage.getItem("session");
	var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + $scope.session + "/date/" + $scope.date + "?format=json";
	console.log(url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			//alert("SUCCESS");
			console.log(data);
			$scope.defaultData = data;
			//$scope.s_date = $scope.date;
			//alert($scope.s_date);
			//$scope.$apply();
			//window.localStorage.setItem('studata', data);
		})
		.error(function (data) {
			$scope.defaultData = '';


			//$scope.radioData = '';
			//window.localStorage.setItem('studata', '');
		});

	$scope.classattend = function (date) {
		console.log(date);
		var s_date = new Date(date),
			mnth = ("0" + (date.getMonth() + 1)).slice(-2),
			day = ("0" + date.getDate()).slice(-2);
		$scope.s_date = [date.getFullYear(), mnth, day].join("-");
		console.log($scope.date);
		$scope.s_date = $scope.s_date;
		$scope.init = '0';
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		$scope.session = localStorage.getItem("session");
		var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + $scope.session + "/date/" + $scope.s_date + "?format=json";
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.defaultData = data;

				//	$scope.$apply();
				//window.localStorage.setItem('studata', data);
			})
			.error(function (data) {
				$scope.defaultData = '';
				//$scope.radioData = '';
				//window.localStorage.setItem('studata', '');
			});
	};

	// Get check_attn_mark_per_day start
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/attn_mark_per_day";
	console.log(url);

	$http.get(url, { params: {} })
		.success(function (data) {
			console.log(data);
			var setting = data[0]['setting'];
			if (setting == '2') { $('.attn_typ').css('display', 'block'); }
			else { $('.attn_typ').css('display', 'none'); }
		})
		.error(function (data) {
		});
	// Get check_attn_mark_per_day END


	$scope.radioChanged = function () {
		var session = $scope.data.group1;
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/getallattendance/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/session/" + session + "/date/" + $scope.s_date + "?format=json";
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert("SUCCESS");
				console.log(data);
				$scope.defaultData = data;
			})
			.error(function (data) {
				$scope.defaultData = '';
			});
	};


	$scope.make_absent = function (studentID) {
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$scope.teacherID = localStorage.getItem("teacherID");
		//alert(studentID);
		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'confirm-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: "Alert",
					content: "Do you want to make absent this student today?",
					ok: "Make Absent",
					cancel: "Cancel"
				}
			}
		}).then(function () {
			//Update single student attendance
			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			$scope.session = localStorage.getItem("session");
			var up_attn_a = 'A';
			var url = $scope.baseurl + "/api/staffAPI/updatesingleattendance/studentid/" + studentID + "/attn_date/" + $scope.s_date + "/attn/" + up_attn_a + "/teacherID/" + $scope.teacherID + "/classID/" + $scope.classID + "/sectionID/" + $scope.sectionID + "/session/" + $scope.sectionID + "?format=json";
			console.log(url);
			$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					//alert("SUCCESS");
					console.log(data);
					var status = data['status'];

					if (status == 1) {
						$mdDialog.show({
							controller: 'DialogController',
							templateUrl: 'alert-dialog.html',
							targetEvent: '',
							locals: {
								displayOption: {
									title: 'Attendance Updated',
									content: "",
									ok: "OK"
								}
							}
						}).then(function () {
							$state.reload();
						});
					} else {
						$mdDialog.show({
							controller: 'DialogController',
							templateUrl: 'alert-dialog.html',
							targetEvent: '',
							locals: {
								displayOption: {
									title: 'Attendance Failed',
									content: "",
									ok: "OK"
								}
							}
						}).then(function () {
							$state.reload();
						});
					}

				})
				.error(function (data) {
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Failed',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();
					});
				});
		});
	};

	$scope.make_present = function (studentID) {

		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$scope.teacherID = localStorage.getItem("teacherID");
		//alert(studentID);
		$mdDialog.show({
			controller: 'DialogController',
			templateUrl: 'confirm-dialog.html',
			targetEvent: '',
			locals: {
				displayOption: {
					title: "Alert",
					content: "Do you want to make present this student today?",
					ok: "Make Present",
					cancel: "Cancel"
				}
			}
		}).then(function () {
			//Update single student attendance
			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			$scope.session = localStorage.getItem("session");
			var up_attn_p = 'P';
			var url = $scope.baseurl + "/api/staffAPI/updatesingleattendance/studentid/" + studentID + "/attn_date/" + $scope.s_date + "/attn/" + up_attn_p + "/teacherID/" + $scope.teacherID + "/classID/" + $scope.classID + "/sectionID/" + $scope.sectionID + "/session/" + $scope.sectionID + "?format=json";
			console.log(url);
			$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					//alert("SUCCESS");
					console.log(data);
					var status = data['status'];
					//alert(status);
					if (status == 1) {
						$mdDialog.show({
							controller: 'DialogController',
							templateUrl: 'alert-dialog.html',
							targetEvent: '',
							locals: {
								displayOption: {
									title: 'Attendance Updated',
									content: "",
									ok: "OK"
								}
							}
						}).then(function () {
							$state.reload();
						});
					} else {
						$mdDialog.show({
							controller: 'DialogController',
							templateUrl: 'alert-dialog.html',
							targetEvent: '',
							locals: {
								displayOption: {
									title: 'Attendance Failed',
									content: "",
									ok: "OK"
								}
							}
						}).then(function () {
							$state.reload();
						});
					}

				})
				.error(function (data) {
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: 'Attendance Failed',
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();
					});
				});

		});

	};


	//}
}); // End of  eidt attendance controller.

// Controller of library page.
appControllers.controller('libraryCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");
	};

	$scope.call_issuedbook = function () {
		$state.go("app.book_issued");
	};

	$scope.call_returnedbook = function () {
		$state.go("app.book_returned");
	};

	$scope.call_requestbook = function () {
		$state.go("app.book_request_list");
	};

	$scope.call_reservebook = function () {
		$state.go("app.book_reserve");
	};



	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  library controller.

// Controller of bookissued page.
appControllers.controller('book_issuedCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");

		//alert($scope.parentID);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://demo.tturk.in/api/parentAPI/getparentnotification/parentid/" + $scope.parentID;
		var url = $scope.baseurl + "/api/staffAPI/getlibrarydetails/teacherid/" + $scope.teacherID;
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				console.log(data);
				$scope.datas = data;

			})
			.error(function (data) {
				$scope.datas = "";
			});


	};



	$ionicModal.fromTemplateUrl('templates/lib_details.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});
	$scope.show = function (id) {
		//alert(id);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		var url = $scope.baseurl + "/api/staffAPI/getsinglelibdetails/id/" + id + "?format=json";
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.single_data = data[0];
			})
			.error(function (data) {
				$scope.single_data = '';
				console.log(data);
			});
		$scope.modal.show();

	};

	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  bookissued controller.

// Controller of book_returned page.
appControllers.controller('book_returnedCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");

		//alert($scope.parentID);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://demo.tturk.in/api/parentAPI/getparentnotification/parentid/" + $scope.parentID;
		var url = $scope.baseurl + "/api/staffAPI/getlibrarydetails/teacherid/" + $scope.teacherID;
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				console.log(data);
				$scope.datas = data;

			})
			.error(function (data) {
				$scope.datas = "";
			});


	};



	$ionicModal.fromTemplateUrl('templates/lib_details.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});
	$scope.show = function (id) {
		//alert(id);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		var url = $scope.baseurl + "/api/staffAPI/getsinglelibdetails/id/" + id + "?format=json";
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.single_data = data[0];
			})
			.error(function (data) {
				$scope.single_data = '';
				console.log(data);
			});
		$scope.modal.show();

	};

	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  book_returned controller.

// Controller of book_request page.
appControllers.controller('book_requestCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data'); //alert('book_request');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");
	};

	$scope.bookrequestsubmit = function (book_name, ISBN, author, year, description) {


		$scope.teacherID = localStorage.getItem("teacherID");
		$scope.username = localStorage.getItem("username");
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		var url = $scope.baseurl + "/api/staffAPI/bookrequest/";
		$http.get(url, { params: { "book_name": book_name, "isbn": ISBN, "author": author, "year": year, "description": description, "teacherid": $scope.teacherID, "username": $scope.username } })
			.success(function (data) {

				console.log(data);
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'confirm-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Book Requested Successfully...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$state.reload();
				});
			})
			.error(function (data) {
				console.log(data);
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'confirm-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Form Error...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$state.reload();
				});
			});


	};
	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  book_request controller.

// Controller of book_reserve_list page.
appControllers.controller('book_request_listCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.username = localStorage.getItem("username");
		$scope.today = new Date();

		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://demo.tturk.in/api/parentAPI/getparentnotification/parentid/" + $scope.parentID;
		var url = $scope.baseurl + "/api/staffAPI/getrequestedbookdetails/username/" + $scope.username;
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				console.log(data);
				$scope.datas = data;

			})
			.error(function (data) {
				$scope.datas = "";
			});

		$scope.call_requestbook = function () {
			$state.go("app.book_request");
		};

	};


	$ionicModal.fromTemplateUrl('templates/lib_details.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});
	$scope.show = function (id) {
		//alert(id);
		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		var url = $scope.baseurl + "/api/staffAPI/getsinglerequestdetails/id/" + id + "?format=json";
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.single_data = data[0];
			})
			.error(function (data) {
				$scope.single_data = '';
				console.log(data);
			});
		$scope.modal.show();

	};
	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  book_reserve_list controller.


// Controller of book_reserve page.
appControllers.controller('book_reserveCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.username = localStorage.getItem("username");
		$scope.today = new Date();

		$scope.baseurl = localStorage.getItem("baseurl");
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		//var url ="http://demo.tturk.in/api/parentAPI/getparentnotification/parentid/" + $scope.parentID;
		var url = $scope.baseurl + "/api/staffAPI/getreservedbookdetails/username/" + $scope.username;
		console.log(url);
		$http.get(url, {
			params: {
				"key1": "value1",
				"key2": "value2"
			}
		})
			.success(function (data) {
				console.log(data);
				$scope.datas = data;

			})
			.error(function (data) {
				$scope.datas = "";
			});

		$scope.call_searchbook = function () {
			$state.go("app.book_search");
		};

	};
	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  book_reserve controller.


// Controller of book_search page.
appControllers.controller('book_searchCtrl', function ($scope, $mdBottomSheet, $http, $mdDialog, $state, $ionicModal) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	var teacherID = localStorage.getItem("teacherID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/teacherid/" + teacherID;
	console.log("URL " + url);
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */


	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");

		$scope.today = new Date();



		$scope.booksearch = function (book_name) {

			$scope.username = localStorage.getItem("username");
			$scope.baseurl = localStorage.getItem("baseurl");
			var url_sec = $scope.baseurl + "/api/staffAPI/getbooksearch/book_name/" + book_name;

			$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					console.log(data);
					$scope.databook = data;
				})
		};

	};


	$ionicModal.fromTemplateUrl('templates/book_reservation.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});
	$scope.show = function (serial_no, book_name) {
		$scope.serial_no = serial_no;
		$scope.book_name = book_name;
		$scope.modal.show();

	};

	$scope.booksubmit = function (reserve_date) {
		//alert('booksubmit');
		//if ($scope.myForm.$valid) {


		$scope.errMessage = '';
		var curDate = new Date();

		if (new Date(reserve_date) > curDate) {
			$scope.teacherID = localStorage.getItem("teacherID");
			$scope.username = localStorage.getItem("username");
			$scope.baseurl = localStorage.getItem("baseurl");
			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			var url = $scope.baseurl + "/api/staffAPI/bookreserve/";
			$http.get(url, { params: { "book_name": $scope.book_name, "serial_no": $scope.serial_no, "reservedate": reserve_date, "teacherid": $scope.teacherID, "username": $scope.username } })
				.success(function (data) {
					$scope.modal.hide();
					console.log(data);
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: "Book Reserved Successfully...!!!",
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();
					});
				})
				.error(function (data) {
					console.log(data);
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'confirm-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: "Form Error...!!!",
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();
					});
				});

		}
		else {
			$scope.errMessage = 'Reserve Date should be greater than today';
			return false;
		}

	};

	//initial load
	$scope.loadData();
	$scope.reloadPage();
}); // End of  book_search controller.

// Controller of assignment page.
appControllers.controller('assignmentCtrl', function ($scope, $state, $http, $mdDialog) {

	var permissions = cordova.plugins.permissions;
	permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);

	function checkPermissionCallback(status) {
		if (!status.hasPermission) {
			var errorCallback = function () {
				//  console.warn('Storage permission is not turned on');
				//    alert("Storage permission is not turned on");
			}
			permissions.requestPermission(
				permissions.READ_EXTERNAL_STORAGE,
				function (status) {
					if (!status.hasPermission) {
						errorCallback();
					} else {
				
					}
				},
				errorCallback);
		} else {

		}
	}
	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var filepath = '';
	localStorage.setItem("fileURL", '');
	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getassignmentclass";

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getassignmentsectionlist/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}

	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getassignmentsectionlist/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {


		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$state.reload();
	};
	/* Top Bar Code End */


	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");

	var date = new Date(),
		mnth = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);
	$scope.date = [date.getFullYear(), mnth, day].join("-");
	$scope.reloadPage = function () {
		//$state.reload(); 
		$scope.reset();
	}

	$scope.reset = function () {
		$scope.details = "";
		$state.reload();
		//$state.go("app.assignment");
	};
	$scope.student = '0';
	$scope.type = "All";
	//$scope.typestu="All";
	//$scope.studentdatas= "";
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/student/class/" + $scope.classID + "/section/" + $scope.sectionID + "?format=json";
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.studentdatas = data;
		})
		.error(function (data) {
			$scope.studentdatas = "";
		});
	var teacherID = localStorage.getItem("teacherID");
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/subject/teacherID/" + teacherID + "?format=json";
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.subjects = data;
		})
		.error(function (data) {
			$scope.subjects = "";
		});
	$scope.radiochange = function (type) {
		//alert(type);

		if (type == 'Specific') {
			$scope.typestu = "Specific";

			$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
			$scope.baseurl = localStorage.getItem("baseurl");
			var url = $scope.baseurl + "/api/staffAPI/student/class/" + $scope.classID + "/section/" + $scope.sectionID + "?format=json";
			$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
				.success(function (data) {
					console.log(data);
					$scope.studentdatas = data;
				})
				.error(function (data) {
					$scope.studentdatas = "";
				});
		} else {
			$scope.typestu = 'All';
			//	$scope.typestu = "";
			//$scope.typestu="All";
			$scope.studentdatas = "";
		}
	};

	$('#fileinput').click(function (e) {
		filechooser.open({}, success, error);

	});

	var success = function (data) {
		var filepath = data.url;
		$scope.fileName = filepath.substr(filepath.lastIndexOf('/') + 1);
		localStorage.setItem("fileURL", filepath);
		$scope.selectedFileName=$scope.fileName ;
		$("#fileName").html('<br><br><div style="color:red;font-size:14px;font-weight:600;text-align:center">'+$scope.selectedFileName+'</div>');

	};

	var error = function (msg) {
		console.log(msg);
	};

	$scope.assignsubmit = function (type, student, subject, details) {
		//alert(type);
		//alert(student);
		//alert(details);
		//	var teacherID='1';

		if (type == null || subject == "undefined" || details == '') {
			//alert('empty');
		}
		else {
			var teacherID = localStorage.getItem("teacherID");
			//var teacherID = 1;
			if (student == null || student == "undefined" || type == 'All') {
				student = '0';
			}


			function win(r) {
				console.log("Code = " + r.responseCode);
				console.log("Response = " + r.response);
				console.log("Sent = " + r.bytesSent);
				console.log(" DATA " + r);

				$scope.reset();

				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'alert-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Assignment Saved Successfully...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					$state.reload();
					$state.go("app.view_assignment");
				});

			}

			function fail(error) {
				console.log("An error has occurred: Code = " + error.code);
				console.log("upload error source " + error.source);
				console.log("upload error target " + error.target);
				$mdDialog.show({
					controller: 'DialogController',
					templateUrl: 'alert-dialog.html',
					targetEvent: '',
					locals: {
						displayOption: {
							title: "Error...!!!",
							content: "",
							ok: "OK"
						}
					}
				}).then(function () {
					//$state.reload();
					$scope.reset();
				});
			}
			var filepath = localStorage.getItem("fileURL");
			$scope.fileName = filepath.substr(filepath.lastIndexOf('/') + 1);
			$scope.baseurl = localStorage.getItem("baseurl");

			var url = $scope.baseurl + "/api/staffAPI/insertassignment/";
			var uri = encodeURI(url);
			var options = new FileUploadOptions();
			options.fileKey = "userfile";
			options.fileName = filepath.substr(filepath.lastIndexOf('/') + 1);
			//	options.mimeType = "multipart/form-data";
			options.httpMethod = "POST";

			var data = new Object()
			options.chunkedMode = false;
			//  options.trustAllHosts = true;
			data.studentID = student;
			data.subjectID = subject;
			data.details = details;
			data.teacherID = teacherID;
			data.type = type;
			data.classID = $scope.classID;
			data.sectionID = $scope.sectionID;
			data.userfile = $scope.fileName;
			options.params = data;

			console.log(student);
			console.log(subject);
			console.log(details);
			console.log(teacherID);
			console.log(type);
			console.log($scope.classID);
			console.log($scope.sectionID);

			var headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
			};

			//	options.headers = headers;
			var ft = new FileTransfer();

			if (filepath.length > 0) {
				ft.upload(filepath, uri, win, fail, options);
			} else {
				$scope.baseurl = localStorage.getItem("baseurl");
				var url = $scope.baseurl + "/api/staffAPI/insertassignment/";
				var options = {
					method: 'post',
					data: { "studentID": student, "subjectID": subject, "details": details, "teacherID": teacherID, "type": type, "classID": $scope.classID, "sectionID": $scope.sectionID },
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				};
				cordova.plugin.http.sendRequest(url, options, function (response) {
					// prints 200
					console.log(response.status);
					$scope.reset();
					$mdDialog.show({
						controller: 'DialogController',
						templateUrl: 'alert-dialog.html',
						targetEvent: '',
						locals: {
							displayOption: {
								title: "Assignment Saved Successfully...!!!",
								content: "",
								ok: "OK"
							}
						}
					}).then(function () {
						$state.reload();
						$state.go("app.view_assignment");
					});
				}, function (response) {
					// prints 403
					console.log("SUCCESS " + response.status);
					//prints Permission denied
					console.log("ERROR " + response.error);
				});

			}
		}
	};
	//}
}); // End of assignment controller.



// Controller of assignment page.
appControllers.controller('view_assignmentCtrl', function ($scope, $mdToast, $mdBottomSheet, $http, $ionicScrollDelegate,
	$ionicSlideBoxDelegate, $timeout, $state, $ionicModal,
	$cordovaFileTransfer, $cordovaFileOpener2, $cordovaFile, $ionicLoading) {

	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	$scope.classname = localStorage.getItem("classname");
	$scope.sectionname = localStorage.getItem("sectionname");

	$scope.teacherID = localStorage.getItem("teacherID");
	$scope.fileName = null;
	$scope.fileURL = null;

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");

	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass/";

	$http.get(url, { params: { "teacherid": $scope.teacherID } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + $scope.teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})

	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + $scope.teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		//var classID = mdSelectValue;
		//var sectionID = mdSelectValue1;

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */

	//if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
	//{

	//$state.go("app.selectclass");
	//}else{
	$timeout(function () {
		$ionicScrollDelegate.scrollBottom();
	});
	// $scope.$index=30;
	$scope.slide = function (to) {
		$scope.current = to;
		$ionicSlideBoxDelegate.slide(to);
	}
	//$ionicScrollDelegate.scrollTo('right', 'top');
	//ShowToast for show toast when user press button.
	var cur = new Date(), setdate = new Date(),
		after30days = cur.setDate(cur.getDate() - 30),
		after3days = new Date().setDate(setdate.getDate() + 2);
	//console.log(new Date(after30days));
	var currentDate = new Date(after30days);
	$scope.dates = [];
	console.log(new Date());
	console.log(currentDate);
	console.log(new Date(after3days));
	while (currentDate <= new Date(after3days)) {


		var yr = currentDate.getFullYear(),
			months = currentDate.getMonth() + 1,
			month = months < 10 ? '0' + months : months,
			day = currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate(),
			days = currentDate.getDay();
		if (days == '0') {
			day_l = 'Sun';
		} if (days == '1') {
			day_l = 'Mon';
		} if (days == '2') {
			day_l = 'Tue';
		} if (days == '3') {
			day_l = 'Wed';
		} if (days == '4') {
			day_l = 'Thu';
		} if (days == '5') {
			day_l = 'Fri';
		} if (days == '6') {
			day_l = 'Sat';
		}

		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
		];

		var d = new Date();
		var monthword = monthNames[months - 1];

		$scope.dates.push({ 'date': day, 'day': day_l, 'full_date': yr + ',' + month + ',' + day, 'monthword': monthword });
		//between.push(new Date(currentDate));
		currentDate.setDate(currentDate.getDate() + 1);
	}
	console.log($scope.dates);
	$scope.getassign = function (yr, mn, d) {
		$scope.current = d;
		//alert($scope.current);
		//var fulldate1=fulldate.split('z');
		// alert(yr+'='+mn+'='+d);
		var mn = mn < 10 ? '0' + mn : mn;
		var date_assign = yr + '-' + mn + '-' + d;
		$scope.ldate = d + '-' + mn + '-' + yr;
		$scope.studentid = localStorage.getItem("studentid");
		//alert($scope.studentid);
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/date/" + date_assign;
		console.log(url);
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				console.log(data);
				$scope.datas = data;
				$scope.msg = '0';
			})
			.error(function (data) {
				$scope.msg = '1';
				$scope.datas = '';
			});

	};
	$scope.showToast = function (menuName) {
		//Calling $mdToast.show to show toast.
		$mdToast.show({
			controller: 'toastController',
			templateUrl: 'toast.html',
			hideDelay: 800,
			position: 'top',
			locals: {
				displayOption: {
					title: 'Going to ' + menuName + " !!"
				}
			}
		});
	}// End showToast.

	$scope.loadData = function () {
		$scope.studentid = localStorage.getItem("studentid");
		$scope.radioval = { studentID: $scope.studentid };


		$scope.studentid = localStorage.getItem("studentid");
		var date = new Date();
		var yr = date.getFullYear(),
			month = date.getMonth() + 1,
			months = month < 10 ? '0' + month : month,
			day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
		var date_assign = yr + '-' + months + '-' + day;
		$scope.date = day + '-' + months + '-' + yr;
		$scope.current = day;
		//alert($scope.studentid);
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/date/" + date_assign;
		//var url = "http://webapp.scoto.in/tturk/api/staffAPI/getteacherassignment/teacherid/2/classID/2/sectionID/10/date/2017-10-15";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.datas = data;
				$scope.msg = '0';
			})
			.error(function (data) {
				$scope.datas = '';
				$scope.msg = '1';

				console.log(data);
			});



	};
	// For close list bottom sheet.

	$scope.call_searchassignment = function () {
		$state.go("app.search_assignment");
	};
	//initial load
	$scope.loadData();

	$ionicModal.fromTemplateUrl('templates/modal.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});


	$scope.redirect = function () {
		// var dir = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
		// $cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
		//     .then(function (success) {
		//         $cordovaFileOpener2.open(
		//             dir,
		//             'application/pdf',
		//             'application/doc',
		//             'application/docx'
		//         ).then(function () {
		//             // file opened successfully
		//         }, function (err) {
		//             // An error occurred. Show a message to the user
		//         });
		//         // success
		//     }, function (error) {
		//         alert(" Please Download the file");
		//     });
	}

	// For close list bottom sheet.
	$scope.download = function () {
		var dir = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;

		var permissions = cordova.plugins.permissions;
		permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkPermissionCallback, null);

		function checkPermissionCallback(status) {
			if (!status.hasPermission) {
				var errorCallback = function () {
					//  console.warn('Storage permission is not turned on');
					//    alert("Storage permission is not turned on");
				}
				permissions.requestPermission(
					permissions.WRITE_EXTERNAL_STORAGE,
					function (status) {
						if (!status.hasPermission) {
							errorCallback();
						} else {
							$ionicLoading.show({
								template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Downloading...'
							});

							$cordovaFile.checkDir(cordova.file.externalRootDirectory, "ttruk")
								.then(function (success) {
									// success
									$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
										.then(function (success) {
											$ionicLoading.hide();
											// success
											$cordovaFileOpener2.open(
												dir,
												'application/pdf',
												'application/doc',
												'application/docx'
											).then(function () {
												// file opened successfully
											}, function (err) {
												// An error occurred. Show a message to the user
											});
										}, function (error) {
											// error
											var url = $scope.fileURL;
											var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
											var trustHosts = true;
											var options = {};
											$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
												.then(function (result) {
													// Success!
													$cordovaFileOpener2.open(
														dir,
														'application/pdf',
														'application/doc',
														'application/docx'
													).then(function () {
														// file opened successfully
													}, function (err) {
														// An error occurred. Show a message to the user
													});
													$ionicLoading.hide();
												}, function (err) {
													// Error
												}, function (progress) {
													$timeout(function () {
														$scope.downloadProgress = (progress.loaded / progress.total) * 100;
													});
												});


										});
								}, function (error) {
									// error
									// CREATE
									$cordovaFile.createDir(cordova.file.externalRootDirectory, "ttruk", false)
										.then(function (success) {
											// success
											$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
												.then(function (success) {
													// success
													$ionicLoading.hide();
													$cordovaFileOpener2.open(
														dir,
														'application/pdf',
														'application/doc',
														'application/docx'
													).then(function () {
														// file opened successfully
													}, function (err) {
														// An error occurred. Show a message to the user
													});
												}, function (error) {
													// error
													var url = $scope.fileURL;
													var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
													var trustHosts = true;
													var options = {};
													$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
														.then(function (result) {
															// Success!
															$cordovaFileOpener2.open(
																dir,
																'application/pdf',
																'application/doc',
																'application/docx'
															).then(function () {
																// file opened successfully
															}, function (err) {
																// An error occurred. Show a message to the user
															});
															$ionicLoading.hide();
														}, function (err) {
															// Error
															$ionicLoading.hide();
														}, function (progress) {
															$timeout(function () {
																$scope.downloadProgress = (progress.loaded / progress.total) * 100;
															});
														});
												});
											// success
										}, function (error) {
											// error
											$ionicLoading.hide();
										});
								});


						}
					},
					errorCallback);
			} else {
				$ionicLoading.show("Download", "Please wait...", true);
				//var url = "https://developer.android.com/static/images/home/ics-android.png";
				// CHECK
				$cordovaFile.checkDir(cordova.file.externalRootDirectory, "ttruk")
					.then(function (success) {
						// success
						$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
							.then(function (success) {
								$ionicLoading.hide();
								// success
								$cordovaFileOpener2.open(
									dir,
									'application/pdf',
									'application/doc',
									'application/docx'
								).then(function () {
									// file opened successfully
								}, function (err) {
									// An error occurred. Show a message to the user
								});
							}, function (error) {
								// error
								var url = $scope.fileURL;
								var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
								var trustHosts = true;
								var options = {};
								$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
									.then(function (result) {
										// Success!
										$ionicLoading.hide();

										$cordovaFileOpener2.open(
											dir,
											'application/pdf',
											'application/doc',
											'application/docx'
										).then(function () {
											// file opened successfully
										}, function (err) {
											// An error occurred. Show a message to the user
										});
									}, function (err) {
										// Error
									}, function (progress) {
										$timeout(function () {
											$scope.downloadProgress = (progress.loaded / progress.total) * 100;
										});
									});


							});
					}, function (error) {
						// error
						// CREATE
						$cordovaFile.createDir(cordova.file.externalRootDirectory, "ttruk", false)
							.then(function (success) {
								// success
								$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
									.then(function (success) {
										// success
										$ionicLoading.hide();

										$cordovaFileOpener2.open(
											dir,
											'application/pdf',
											'application/doc',
											'application/docx'
										).then(function () {
											// file opened successfully
										}, function (err) {
											// An error occurred. Show a message to the user
										});
									}, function (error) {
										// error
										var url = $scope.fileURL;
										var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
										var trustHosts = true;
										var options = {};
										$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
											.then(function (result) {
												// Success!
												$ionicLoading.hide();

												$cordovaFileOpener2.open(
													dir,
													'application/pdf',
													'application/doc',
													'application/docx'
												).then(function () {
													// file opened successfully
												}, function (err) {
													// An error occurred. Show a message to the user
												});
											}, function (err) {
												// Error
											}, function (progress) {
												$timeout(function () {
													$scope.downloadProgress = (progress.loaded / progress.total) * 100;
												});
											});
									});
								// success
							}, function (error) {
								// error                                    
							});
					});
			}
		}
	}

	$scope.show = function (id) {

		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/parentAPI/getsingleassignment/id/" + id + "?format=json";
		console.log(url);
		//var url ="http://webapp.scoto.in/tturk/api/parentAPI/getsingleassignment/id/"+id+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.single_data = data[0];
				$scope.content = data[0]['content'];
				$scope.fileName = data[0]['file'];
				$("#content").html($scope.content);
				if ($scope.fileName == 'Empty') {
					$scope.filefound = '0';
					console.log("FILE EMPTY");
				} else {
					console.log("FILE NOT EMPTY");
					$scope.filefound = '1';
				}
				$scope.fileURL = $scope.baseurl + "/uploads/assignment/" + $scope.fileName;

				console.log($scope.content);
			})
			.error(function (data) {
				$scope.single_data = '';
				console.log(data);
			});
		$scope.modal.show();

	};
	//}
});// End of controller assignment.


// Controller of book_search page.
appControllers.controller('search_assignmentCtrl', function ($scope, $mdToast, $mdBottomSheet, $http, $ionicScrollDelegate,
	$ionicSlideBoxDelegate, $timeout, $state, $ionicModal, $filter, $cordovaFileTransfer, $cordovaFileOpener2, $cordovaFile, $ionicLoading) {
	console.log('data');
	$scope.reloadPage = function () {
		//$state.reload();
	}
	var permissions = cordova.plugins.permissions;
	permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);

	function checkPermissionCallback(status) {
		if (!status.hasPermission) {
			var errorCallback = function () {
				//  console.warn('Storage permission is not turned on');
				//    alert("Storage permission is not turned on");
			}
			permissions.requestPermission(
				permissions.READ_EXTERNAL_STORAGE,
				function (status) {
					if (!status.hasPermission) {
						errorCallback();
					} else {
				
					}
				},
				errorCallback);
		} else {

		}
	}

	/* Top Bar Code start */
	var classID = localStorage.getItem("classID");
	var sectionID = localStorage.getItem("sectionID");
	var teacherID = localStorage.getItem("teacherID");
	$scope.fileName = null;
	$scope.fileURL = null;
	$scope.mdSelectValue = classID;
	$scope.mdSelectValue1 = sectionID;

	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getallclass";

	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {

			$(".dash_content").css('display', 'block');
			$(".dash_content_warning").html('');
			console.log('data');
			console.log(data);
			$scope.datas_class = data;
			console.log($scope.datas_class);
		})
		.error(function (data) {
			$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
		});

	if (classID != null) {
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + classID;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}



	$scope.getsection = function (chrf) {
		$scope.mdSelectValue1 = '';
		$scope.datasec = '';
		$scope.baseurl = localStorage.getItem("baseurl");
		var url_sec = $scope.baseurl + "/api/staffAPI/getsectionlist/teacherid/" + teacherID+"/classid/" + chrf;

		$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				$scope.status = data[0]['status'];
				$scope.datasec = data;
			})
	}; // fn clos


	$scope.proceed = function (mdSelectValue, mdSelectValue1) {

		var classID = mdSelectValue;
		var sectionID = mdSelectValue1;

		localStorage.setItem("classID", mdSelectValue);
		localStorage.setItem("sectionID", mdSelectValue1);
		$scope.classID = localStorage.getItem("classID");
		$scope.sectionID = localStorage.getItem("sectionID");
		$state.reload();
		$scope.loadData();
	};
	/* Top Bar Code End */

	$scope.loadData = function () {
		$scope.teacherID = localStorage.getItem("teacherID");

		$scope.today = new Date();
		var date = new Date();
		var yr = date.getFullYear(),
			month = date.getMonth() + 1,
			months = month < 10 ? '0' + month : month,
			day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
		var date_assign = yr + '-' + months + '-' + day;
		$scope.date = day + '-' + months + '-' + yr;

		$scope.datesubmit = function (date) {

			if (date != null) {
				console.log(date);
				var s_date = new Date(date),
					mnth = ("0" + (date.getMonth() + 1)).slice(-2),
					day = ("0" + date.getDate()).slice(-2);
				$scope.s_date = [date.getFullYear(), mnth, day].join("-");
				console.log($scope.s_date);
				$scope.s_date = $scope.s_date;

				$scope.classID = localStorage.getItem("classID");
				$scope.sectionID = localStorage.getItem("sectionID");
				$scope.teacherID = localStorage.getItem("teacherID");

				$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
				$scope.baseurl = localStorage.getItem("baseurl");
				var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/date/" + $scope.s_date;
				console.log(url);
				$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
					.success(function (data) {
						console.log(data);
						$scope.data_assn = data;
						console.log($scope.data_assn);
						$scope.msg = '0';
					})
					.error(function (data) {
						$scope.msg = '1';
						$scope.datas = '';
					});
			}
			else {
				//alert('err');
			}

		};

		$scope.predatesubmit = function (date, s_date) {
			alert(s_date);
			if (date != null) {
				console.log(date);
				var s_date = new Date(date),
					mnth = ("0" + (date.getMonth() + 1)).slice(-2),
					day = ("0" + date.getDate()).slice(-2);
				$scope.s_date = [date.getFullYear(), mnth, day].join("-");
				var pre_timestamp = s_date.setDate(s_date.getDate() - 1);
				var pre_full = new Date(pre_timestamp);
				var pre = new Date(pre_full),
					premnth = ("0" + (pre_full.getMonth() + 1)).slice(-2),
					preday = ("0" + pre_full.getDate()).slice(-2);
				$scope.pre_date = [pre_full.getFullYear(), premnth, preday].join("-");
				console.log($scope.pre_date);
				$scope.s_date = $scope.pre_date;

				$scope.classID = localStorage.getItem("classID");
				$scope.sectionID = localStorage.getItem("sectionID");
				$scope.teacherID = localStorage.getItem("teacherID");

				$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
				$scope.baseurl = localStorage.getItem("baseurl");
				var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/date/" + $scope.s_date;
				console.log(url);
				$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
					.success(function (data) {
						console.log(data);
						$scope.data_assn = data;
						
						console.log($scope.data_assn);
						$scope.msg = '0';
					})
					.error(function (data) {
						$scope.msg = '1';
						$scope.datas = '';
					});


			}
			else {
				//alert('err');
			}

		};


		$scope.nextdatesubmit = function (date, s_date) {
			//alert(s_date);
			if (date != null) {
				console.log(date);
				var s_date = new Date(date),
					mnth = ("0" + (date.getMonth() + 1)).slice(-2),
					day = ("0" + date.getDate()).slice(-2);
				$scope.s_date = [date.getFullYear(), mnth, day].join("-");
				var pre_timestamp = s_date.setDate(s_date.getDate() + 1);
				var pre_full = new Date(pre_timestamp);
				var pre = new Date(pre_full),
					premnth = ("0" + (pre_full.getMonth() + 1)).slice(-2),
					preday = ("0" + pre_full.getDate()).slice(-2);
				$scope.pre_date = [pre_full.getFullYear(), premnth, preday].join("-");
				console.log($scope.pre_date);
				$scope.s_date = $scope.pre_date;

				$scope.classID = localStorage.getItem("classID");
				$scope.sectionID = localStorage.getItem("sectionID");
				$scope.teacherID = localStorage.getItem("teacherID");

				$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
				$scope.baseurl = localStorage.getItem("baseurl");
				var url = $scope.baseurl + "/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherID + "/classid/" + $scope.classID + "/sectionid/" + $scope.sectionID + "/date/" + $scope.s_date;
				console.log(url);
				$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
					.success(function (data) {
						console.log(data);
						$scope.data_assn = data;
						console.log($scope.data_assn);
						$scope.msg = '0';
					})
					.error(function (data) {
						$scope.msg = '1';
						$scope.datas = '';
					})
			}
			else {
				//alert('err');
			}
		};

	};

	$ionicModal.fromTemplateUrl('templates/modal.html', {
		scope: $scope
	}).then(function (modal) {
		$scope.modal = modal;
	});


	//initial load
	$scope.loadData();
	//$scope.reloadPage();
	$scope.redirect = function () {
		var dir = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
		$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
			.then(function (success) {
				$cordovaFileOpener2.open(
					dir,
					'application/pdf',
					'application/doc',
					'application/docx'
				).then(function () {
					// file opened successfully
				}, function (err) {
					// An error occurred. Show a message to the user
				});
				// success
			}, function (error) {
				alert(" Please Download the file");
			});


	}

	// For close list bottom sheet.
	$scope.download = function () {
		var dir = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;

		var permissions = cordova.plugins.permissions;
		permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, checkPermissionCallback, null);

		function checkPermissionCallback(status) {
			if (!status.hasPermission) {
				var errorCallback = function () {
					//  console.warn('Storage permission is not turned on');
					//    alert("Storage permission is not turned on");
				}
				permissions.requestPermission(
					permissions.WRITE_EXTERNAL_STORAGE,
					function (status) {
						if (!status.hasPermission) {
							errorCallback();
						} else {
							$ionicLoading.show({
								template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Downloading...'
							});

							$cordovaFile.checkDir(cordova.file.externalRootDirectory, "ttruk")
								.then(function (success) {
									// success
									$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
										.then(function (success) {
											$ionicLoading.hide();
											// success
											$cordovaFileOpener2.open(
												dir,
												'application/pdf',
												'application/doc',
												'application/docx'
											).then(function () {
												// file opened successfully
											}, function (err) {
												// An error occurred. Show a message to the user
											});
										}, function (error) {
											// error
											var url = $scope.fileURL;
											var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
											var trustHosts = true;
											var options = {};
											$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
												.then(function (result) {
													// Success!

													$ionicLoading.hide();
													$cordovaFileOpener2.open(
														dir,
														'application/pdf',
														'application/doc',
														'application/docx'
													).then(function () {
														// file opened successfully
													}, function (err) {
														// An error occurred. Show a message to the user
													});
												}, function (err) {
													// Error
												}, function (progress) {
													$timeout(function () {
														$scope.downloadProgress = (progress.loaded / progress.total) * 100;
													});
												});


										});
								}, function (error) {
									// error
									// CREATE
									$cordovaFile.createDir(cordova.file.externalRootDirectory, "ttruk", false)
										.then(function (success) {
											// success
											$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
												.then(function (success) {
													$ionicLoading.hide();
													// success
													$cordovaFileOpener2.open(
														dir,
														'application/pdf',
														'application/doc',
														'application/docx'
													).then(function () {
														// file opened successfully
													}, function (err) {
														// An error occurred. Show a message to the user
													});
												}, function (error) {
													// error
													var url = $scope.fileURL;
													var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
													var trustHosts = true;
													var options = {};
													$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
														.then(function (result) {
															$ionicLoading.hide();
															// Success!
															$cordovaFileOpener2.open(
																dir,
																'application/pdf',
																'application/doc',
																'application/docx'
															).then(function () {
																// file opened successfully
															}, function (err) {
																// An error occurred. Show a message to the user
															});
														}, function (err) {
															// Error
														}, function (progress) {
															$timeout(function () {
																$scope.downloadProgress = (progress.loaded / progress.total) * 100;
															});
														});


												});
											// success
										}, function (error) {
											// error                                    
										});
								});




						}
					},
					errorCallback);
			} else {
				$ionicLoading.show("Download", "Please wait...", true);
				//var url = "https://developer.android.com/static/images/home/ics-android.png";
				// CHECK
				$cordovaFile.checkDir(cordova.file.externalRootDirectory, "ttruk")
					.then(function (success) {
						// success
						$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
							.then(function (success) {
								$ionicLoading.hide();
								// success

								$cordovaFileOpener2.open(
									dir,
									'application/pdf',
									'application/doc',
									'application/docx'
								).then(function () {
									// file opened successfully
								}, function (err) {
									// An error occurred. Show a message to the user
								});
							}, function (error) {
								// error
								var url = $scope.fileURL;
								var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
								var trustHosts = true;
								var options = {};
								$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
									.then(function (result) {
										// Success!
										$ionicLoading.hide();
										$cordovaFileOpener2.open(
											dir,
											'application/pdf',
											'application/doc',
											'application/docx'
										).then(function () {
											// file opened successfully
										}, function (err) {
											// An error occurred. Show a message to the user
										});
									}, function (err) {
										// Error
										$ionicLoading.hide();
									}, function (progress) {
										$timeout(function () {
											$scope.downloadProgress = (progress.loaded / progress.total) * 100;
										});
									});


							});
					}, function (error) {
						// error
						// CREATE
						$cordovaFile.createDir(cordova.file.externalRootDirectory, "ttruk", false)
							.then(function (success) {
								// success
								$cordovaFile.checkFile(cordova.file.externalRootDirectory, "ttruk/" + $scope.fileName)
									.then(function (success) {
										// success
										$ionicLoading.hide();
										$cordovaFileOpener2.open(
											dir,
											'application/pdf',
											'application/doc',
											'application/docx'
										).then(function () {
											// file opened successfully
										}, function (err) {
											// An error occurred. Show a message to the user
										});
									}, function (error) {
										// error
										var url = $scope.fileURL;
										var targetPath = cordova.file.externalRootDirectory + "/ttruk/" + $scope.fileName;
										var trustHosts = true;
										var options = {};
										$cordovaFileTransfer.download(url, targetPath, options, trustHosts)
											.then(function (result) {
												// Success!
												$ionicLoading.hide();
												$cordovaFileOpener2.open(
													dir,
													'application/pdf',
													'application/doc',
													'application/docx'
												).then(function () {
													// file opened successfully
												}, function (err) {
													// An error occurred. Show a message to the user
												});
											}, function (err) {
												// Error
												$ionicLoading.hide();
											}, function (progress) {
												$timeout(function () {
													$scope.downloadProgress = (progress.loaded / progress.total) * 100;
												});
											});
									});
								// success
							}, function (error) {
								// error                                    
							});
					});
			}
		}
	}

	$scope.show = function (id) {

		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
		$scope.baseurl = localStorage.getItem("baseurl");
		var url = $scope.baseurl + "/api/parentAPI/getsingleassignment/id/" + id + "?format=json";
		console.log(url);
		//var url ="http://webapp.scoto.in/tturk/api/parentAPI/getsingleassignment/id/"+id+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
			.success(function (data) {
				//alert(data);
				console.log(data);
				$scope.single_data = data[0];
				$scope.content = data[0]['content'];
				$scope.fileName = data[0]['file'];
				$("#content").html($scope.content);
				if ($scope.fileName == 'Empty') {
					$scope.fileFound = '0';
					console.log("FILE EMPTY");
				} else {
					console.log("FILE NOT EMPTY");
					$scope.fileFound = '1';
				}
				$scope.fileURL = $scope.baseurl + "/uploads/assignment/" + $scope.fileName;
				console.log($scope.content);
			})
			.error(function (data) {
				$scope.single_data = '';
				console.log(data);
			});
		$scope.modal.show();

	};



}); // End of  assignment_search controller.

// Controller of timetable page.
appControllers.controller('timetableCtrl', function ($scope, $state, $timeout, $ionicScrollDelegate, $ionicSlideBoxDelegate, $http) {
	$scope.teacherID = localStorage.getItem("teacherID");
	//$scope.teacherID = '1';

	$timeout(function () {
		// $ionicScrollDelegate.scrollBottom();
	});

	$scope.slide = function (to) {
		$scope.currentday = to;
		$ionicSlideBoxDelegate.slide(to);
	}

	var d = new Date();
	var weekday = new Array("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY");
	$scope.currentday = weekday[d.getDay()];
	$scope.activeMenu = $scope.currentday;
	//$scope.activeMenu = "SATURDAY";	

	$scope.dayNames = ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"];


	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' + ':' + '1234');
	$scope.baseurl = localStorage.getItem("baseurl");
	var url = $scope.baseurl + "/api/staffAPI/getstafftimetable/teacherid/" + $scope.teacherID;
	$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function (data) {
			console.log(data);
			$scope.datas = data;

			var datas1 = $scope.datas;
			console.log(datas1);
			console.log($scope.currentday);
			//var api_day1 = [];
			var api_day2 = [];
			for (var i = 0; i < datas1.length; i++) {


				var hour = datas1[i]['HOUR'];
				$scope.ap_currentday = $scope.currentday.substring(0, 3);
				var valuu = datas1[i][$scope.ap_currentday];
				var start_time = datas1[i]['start_time'];
				var end_time = datas1[i]['end_time'];
				//alert(start_time);
				if (valuu != 'Empty') {

					api_day2.push({
						"hour": hour,
						"class": valuu,
						"start_time": start_time,
						"end_time": end_time
					});

				}

			}

			console.log(api_day2);
			api_day2 = api_day2.filter(function (v) { return v !== null });
			$scope.api_length = api_day2.length;
			console.log(api_day2.length);
			$scope.api_dayvalue = api_day2;

		})
		.error(function (data) {

		});

	$scope.setActive_gettimetable = function (menuItem) {

		$scope.activeMenu = menuItem;
		$scope.day = menuItem.substring(0, 3);
		//alert($scope.day);
		//console.log($scope.datas);
		var datas = $scope.datas;
		var api_day = [];
		for (var i = 0; i < datas.length; i++) {
			var val = datas[i][$scope.day];
			var hour = datas[i]['HOUR'];
			var start_time = datas[i]['start_time'];
			var end_time = datas[i]['end_time'];

			if (val != 'Empty') {
				api_day.push({
					"hour": hour,
					"class": val,
					"start_time": start_time,
					"end_time": end_time
				});

			}
		}
		console.log(api_day);
		console.log(api_day.length);
		$scope.api_length = api_day.length;
		$scope.api_dayvalue = api_day;
	}

}); // End of timetable controller.


// Controller of settlement page.
appControllers.controller('logoutCtrl', function ($scope, $rootScope, $timeout, $ionicHistory, $state) {
	window.localStorage.clear();
	$ionicHistory.clearCache();
	$ionicHistory.clearHistory();
	localStorage.setItem("baseurl","");
	console.log(" BASE URL " + $scope.baseurl)
	$state.go("app.home");


}); // End of  settlement controller.