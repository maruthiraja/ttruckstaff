var appControllers = angular.module('starter.controllers', ['ngCordova']); // Use for all controller of application.
var appServices = angular.module('starter.services', []);// Use for all service of application.


// Controller of home page.
appControllers.controller('homeCtrl', function ($scope,$rootScope , $state,$stateParams, $ionicHistory, $mdDialog, $timeout) {
	$scope.isAnimated =  $stateParams.isAnimated;
//$rootScope.value='Its parent';
//$state.reload();
	
	$scope.navigateTo = function (stateName) {
        $timeout(function () {
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: false,
                    disableBack: true
                }); 
                $state.go(stateName);
            }
        }, ($scope.isAnimated  ? 300 : 0));
    }; // End of navigateTo.

	//alert('home ctrl');
	$timeout( function(){ 
		alert('home refresh'); 
			$state.go("app.login"); }, 1000);
	//$state.go("app.login");
}); // End of  home controller.

appControllers.controller('loginCtrl', function ($scope,$rootScope, $timeout, $state,$stateParams, $ionicHistory, $mdDialog, $http) {
//alert('start');
//console.log($rootScope.value);
//$state.reload();
    //$scope.isAnimated is the variable that use for receive object data from state params.
    //For enable/disable row animation.
    $scope.isAnimated =  $stateParams.isAnimated;

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go.
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: false,
                    disableBack: true
                });
                $state.go(stateName);
            }
        }, ($scope.isAnimated  ? 300 : 0));
    }; // End of navigateTo.

/*
$scope.username = localStorage.getItem("username");
if($scope.username != null)
{
alert($scope.username);
$state.go("app.dashboard");
}
else
{
	*/
$state.go("app.login");
var nfcid = nfc.addTagDiscoveredListener(
function (nfcEvent) {
	 var tag = nfcEvent.tag;
     var tagId = nfc.bytesToHexString(tag.id);
       alert(tagId);

$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/vendorAPI/getcardtype/nfcid/" + tagId;
//var url ="http://scoto.in/app/tturk/v1/api/vendorAPI/getavailableamt/nfcid/fa124405";

$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
.success(function(data) {
var user = data[0]['user'];
alert(user);
if(user == 'Teacher' )
{

							$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
							var url ="http://scoto.in/app/tturk/v1/api/staffAPI/teacherlogin/card/" + tagId;
							//var url ="http://scoto.in/app/tturk/v1/api/vendorAPI/getavailableamt/nfcid/fa124405";

							$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
							.success(function(data) {
								var teacherID = data[0]['teacherID'];
								var name = data[0]['name'];
								//alert(vendorID);

								localStorage.setItem("teacherID", teacherID);
								localStorage.setItem("name", name);
								$state.go("app.dashboard");

							nfc.removeTagDiscoveredListener(
							function () { 
								//alert('removed'); 
								},
							function () { // success callback
								//alert("tag removed");
							},
							function (error) { // error callback
								alert("tag Error");
							}
							);

							})
							.error(function(data) {
								$mdDialog.show({
								controller: 'DialogController',
								templateUrl: 'confirm-dialog.html',
								targetEvent: '',
								locals: {
								displayOption: {
								title: "Vendor Access Denied...!!!",
								content: "",
								ok: "OK"
								}
								}
								}).then(function () {
								$state.go("app.login");
								});
							});
}
else
{

} // else close

})
.error(function(data) {
	$mdDialog.show({
	controller: 'DialogController',
	templateUrl: 'confirm-dialog.html',
	targetEvent: '',
	locals: {
	displayOption: {
	title: "Connection Error...!!!",
	content: "",
	ok: "OK"
	}
	}
	}).then(function () {
	$state.go("app.login");
	});
});

 },
function () { // success callback
	alert("Listening for NFC Tags vendor nfc ");
},
function (error) { // error callback
    alert("Error adding NDEF listener " + JSON.stringify(error));
}
);

//}//else close


}); // End of  login controller.


// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('menuCtrl', function ($scope, $timeout, $mdUtil, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect) {
    
    $scope.toggleLeft = buildToggler('left');

	$scope.teacherID = localStorage.getItem("teacherID");
	$scope.name = localStorage.getItem("name");

    // buildToggler is for create menu toggle.
    // Parameter :  
    // navID = id of navigation bar.
    function buildToggler(navID) {
        var debounceFn = $mdUtil.debounce(function () {
            $mdSidenav(navID).toggle();
			$scope.teacherID = localStorage.getItem("teacherID");
			$scope.name = localStorage.getItem("name");
        }, 0);
        return debounceFn;
    };// End buildToggler.

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            $mdSidenav('left').close();
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
				$scope.teacherID = localStorage.getItem("teacherID");
			$scope.vendorname = localStorage.getItem("vendorname");
                $state.go(stateName);
            }
        }, ($scope.isAndroid == false ? 300 : 0));
    };// End navigateTo.

    //closeSideNav is for close side navigation
    //It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
    //When user swipe or drag md-sidenav to left side
    $scope.closeSideNav = function(){
        $mdSidenav('left').close();
		$scope.teacherID = localStorage.getItem("teacherID");
			$scope.name = localStorage.getItem("name");
    };
    //End closeSideNav

    $ionicPlatform.registerBackButtonAction(function(){

        if($mdSidenav("left").isOpen()){
            //If side navigation is open it will close and then return
            $mdSidenav('left').close();
        }
        else if(jQuery('md-bottom-sheet').length > 0 ) {
            //If bottom sheet is open it will close and then return
            $mdBottomSheet.cancel();
        }
        else if(jQuery('[id^=dialog]').length > 0 ){
            //If popup dialog is open it will close and then return
            $mdDialog.cancel();
        }
        else if(jQuery('md-menu-content').length > 0 ){
            //If md-menu is open it will close and then return
            $mdMenu.hide();
        }
        else if(jQuery('md-select-menu').length > 0 ){
            //If md-select is open it will close and then return
            $mdSelect.hide();
        }

        else{
            // Check for the current state that not have previous state.
            // It will show $mdDialog to ask for Confirmation to close the application.

            if($ionicHistory.backView() == null){

                //Check is popup dialog is not open.
                if(jQuery('[id^=dialog]').length == 0 ) {

                    // mdDialog for show $mdDialog to ask for
                    // Confirmation to close the application.

                    $mdDialog.show({
                        controller: 'DialogController',
                        templateUrl: 'confirm-dialog.html',
                        targetEvent: null,
                        locals: {
                            displayOption: {
                                title: "Confirmation",
                                content: "Do you want to close the application?",
                                ok: "Confirm",
                                cancel: "Cancel"
                            }
                        }
                    }).then(function () {
                        //If user tap Confirm at the popup dialog.
                        //Application will close.
                        ionic.Platform.exitApp();
                    }, function () {
                        // For cancel button actions.
                    }); //End mdDialog
                }
            }
            else{
                //Go to the view of lasted state.
                $ionicHistory.goBack();
            }
        }

    },100);
    //End of $ionicPlatform.registerBackButtonAction
}); // End of menu toggle controller.

// Controller of dashboard page.
appControllers.controller('dashbCtrl', function ($scope, $rootScope, $state, $mdDialog, $http) {
	alert('dashbCtrl');
//$state.reload();
$scope.reloadPage = function(){  $state.reload();  }


// For simulator
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getallclass";

$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {

	$(".dash_content").css('display','block');
	$(".dash_content_warning").html('');
	$scope.datas= data;
	//localStorage.setItem("allclass", data);
	})
    .error(function(data) {
		//$(".dash_content").css('display','block');
		$(".dash_content_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Connection Error...!!!</div>');
	});

//For simulator end

$scope.getsection = function(chrf) {
       // alert(chrf);
	$scope.mdSelectValue1 = ''; 
	$scope.datasec= '';
var url_sec ="http://scoto.in/app/tturk/v1/api/staffAPI/getsectionlist/classid/"+chrf;

$http.get(url_sec, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {
	$scope.status = data[0]['status'];
	$scope.datasec= data;
	})
}; // fn close


//$scope.submit_pay = function(p_amt) {
$scope.proceed = function(mdSelectValue,mdSelectValue1) {

//alert($scope.p_amt);
var classID = $scope.mdSelectValue;
var sectionID = $scope.mdSelectValue1;
//alert(mdSelectValue1);
if(mdSelectValue != null && mdSelectValue1 != null && mdSelectValue1 != '' )
{ 
//alert(mdSelectValue);
//alert(mdSelectValue1);
localStorage.setItem("classID", mdSelectValue); 
localStorage.setItem("sectionID", mdSelectValue1);

// Get Class name based on Class ID
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getallclass/classid/"+mdSelectValue;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {
	$scope.classname= data[0].classes;
	localStorage.setItem("classname", $scope.classname);
})

// Get Section name based on Section ID
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getsectionlist/classid/"+mdSelectValue+"/sectionid/"+mdSelectValue1;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {
	console.log(data);
	$scope.sectionname= data[0].section;
	localStorage.setItem("sectionname", $scope.sectionname);
})
//$state.go("app.mark_attendance");
}
else
{
	alert('Must Select Class & Section');
}

}
}); // End of  dashboard controller.


// Controller of mark attendance page.
appControllers.controller('mark_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http) {

$scope.reloadPage = function(){  $state.reload();  }
$scope.date = new Date();
$scope.classID = localStorage.getItem("classID");
$scope.sectionID = localStorage.getItem("sectionID");
$scope.classname = localStorage.getItem("classname");
$scope.sectionname = localStorage.getItem("sectionname");
 $scope.isDisabled = true;

if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
{

$state.go("app.dashbroad");
}else{
    // For show Grid Bottom Sheet.
    $scope.showGridBottomSheet = function ($event) {
        $mdBottomSheet.show({
            templateUrl: 'ui-grid-bottom-sheet-template',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showGridBottomSheet.

	 $scope.doSecondaryAction = function(event) {
    $mdDialog.show(
      $mdDialog.alert()
        .title('Secondary Action')
        .textContent('Secondary actions can be used for one click actions')
        .ariaLabel('Secondary click demo')
        .ok('Neat!')
        .targetEvent(event)
    );
  };


$scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.person, function (item) {
            person.Selected = $scope.selectedAll;
        });

    };


var classID = localStorage.getItem("classID");
var sectionID = localStorage.getItem("sectionID");


var staffID = localStorage.getItem("staffid");
//var staffID = '1';

// Get Class name based on Class ID
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getallclass/classid/"+classID;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {
	$scope.classname= data[0].classes;
	localStorage.setItem("classname", $scope.classname);
})

// Get Section name based on Section ID
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getsectionlist/classid/"+classID+"/sectionid/"+sectionID;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
	.success(function(data) {
	console.log(data);
	$scope.sectionname= data[0].section;
	localStorage.setItem("sectionname", $scope.sectionname);
})

$scope.getCheckedFalse = function(){
    return false;
};

$scope.getCheckedTrue = function(){
    return true;
};

// Get Student attendance list start
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getreceiverdata/class/"+classID+"/section/"+sectionID;
console.log(url);

$http.get(url, { params: {  } })
.success(function(data) {
	console.log(data);
	//$rootScope.name = data[0]['name'];
	//console.log(data[0]['name']);
	$scope.presents= data[0];
	$scope.absents= data[1];
	//console.log(data.message);
	$scope.items = data[1];
	$scope.presents = data[0];
})
.error(function(data) {
	$mdDialog.show({
		controller: 'DialogController',
		templateUrl: 'confirm-dialog.html',
		targetEvent: '',
		locals: {
			displayOption: {
			title: 'Data Not Found',
			content: "",
			ok: "OK"
			}
			}
		}).then(function () {
			//$state.reload(); 
			//$(".dash_content").css('display','none');
			//$window.location.reload(); 
		});	
}); 
// Get Student attendance list end

/*
$scope.folder = {};

$scope.getAllSelected = function(){    
$scope.albumNameArray = [];
	angular.forEach($scope.folder,function(key,value){
    if(key)
                $scope.albumNameArray.push(value)
    });

//alert($scope.albumNameArray);
//console.log($scope.albumNameArray);

//alert(classID);
//alert(sectionID);

$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
			var url ="http://scoto.in/app/tturk/v1/api/staffAPI/attendance_register/";
			var datas = $scope.albumNameArray;
			//console.log(datas);

			var str_obj = JSON.stringify(datas);
			
			//console.log(str_obj);
			$http.get(url, { params: { "data": str_obj,"classID": classID, "sectionID": sectionID, "staffID": staffID } })
			.success(function(data) {
				console.log(data);

			})
			.error(function(data) {
			});
}

*/
//$scope.items = data[1];
//$scope.items = [1,2,3,4,5];
 $scope.selected = [];
  $scope.studentID = [];
  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  $scope.isIndeterminate = function() {
    return ($scope.selected.length !== 0 &&
        $scope.selected.length !== $scope.items.length);
  };

  $scope.isChecked = function() {
    return $scope.selected.length === $scope.items.length;
  };

  $scope.toggleAll = function() {
    if ($scope.selected.length === $scope.items.length) {
      $scope.selected = [];
	  $scope.studentID = [];
    } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      $scope.selected = $scope.items.slice(0);
	   $scope.isDisabled = false;
	  //$scope.datas = angular.copy($scope.items).slice(0, 3);
	   //$scope.studentID = $scope.datas[-1]['studentID'];
    }
  };

$scope.mark_attendance = function() {
	   $scope.isDisabled = true;
	  $arr_val = $scope.selected;
	  $arr_length = $arr_val.length;
	  console.log($scope.selected);
	  console.log($arr_length);
	  var sdata_obj = JSON.stringify($scope.selected);
	  console.log(sdata_obj);
	  $get_value = [];
	  for($i=0;$i<$arr_length;$i++)
	  {
		  $get_val = $arr_val[$i]['studentID'];
		  $get_value.push($get_val);
	  }
	   console.log($get_value);
      $scope.prvalue = $get_value;

 $arr_val1 = $scope.selected1;
	  $arr_length1 = $arr_val1.length;
	  console.log($scope.selected1);
	  console.log($arr_length1);
	  var sdata_obj1 = JSON.stringify($scope.selected1);
	  console.log(sdata_obj1);
	  $get_value1 = [];
	  for($i=0;$i<$arr_length1;$i++)
	  {
		  $get_val1 = $arr_val1[$i]['studentID'];
		  $get_value1.push($get_val1);
	  }
	   console.log($get_value1);
      $scope.abvalue = $get_value1;

 $c = $scope.prvalue.concat( $scope.abvalue );
 console.log($c);

 $http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
			var url ="http://scoto.in/app/tturk/v1/api/staffAPI/attendance_register/";
			var datas = $c;
			console.log(datas);

			
			var str_obj = JSON.stringify(datas);
			
			console.log(str_obj);
			$http.get(url, { params: { "data": str_obj,"classID": classID, "sectionID": sectionID, "staffID": staffID } })
			.success(function(data) {
				console.log(data);

			})
			.error(function(data) {
			});

 };

$scope.selected1 = [];
  $scope.studentID1 = [];
  $scope.toggle1 = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };

  $scope.exists1 = function (item, list) {
    return list.indexOf(item) > -1;
  }; 
    
  $scope.isIndeterminate1 = function() {
    return ($scope.selected1.length !== 0 &&
        $scope.selected1.length !== $scope.presents.length);
  };

  $scope.isChecked1 = function() {
    return $scope.selected1.length === $scope.presents.length;
  };

  $scope.toggleAll1 = function() {
	   $scope.isDisabled = false;
    if ($scope.selected1.length === $scope.presents.length) {
      $scope.selected1 = [];
	  $scope.studentID1 = [];
    } else if ($scope.selected1.length === 0 || $scope.selected1.length > 0) {
      $scope.selected1 = $scope.presents.slice(0);
	  var sdata  = $scope.presents.slice(0);
    }
  };

}
}); // End of  mark attendance controller.




// Controller of view attendance page.
appControllers.controller('view_attendanceCtrl', function ($scope, $mdDialog, $mdBottomSheet, $timeout, $state, $http) {
$scope.classID = localStorage.getItem("classID");
$scope.sectionID = localStorage.getItem("sectionID");
$scope.classname = localStorage.getItem("classname");
$scope.sectionname = localStorage.getItem("sectionname");


$scope.reloadPage = function(){  $state.reload();  }

if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
{

$state.go("app.dashbroad");
}else{
//$scope.defaultData = '';
var date=new Date(),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    $scope.date=[ date.getFullYear(), mnth, day ].join("-");
//	$scope.date= '2016-12-28';
console.log($scope.date);
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getallattendance/classid/"+$scope.classID+"/sectionid/"+$scope.sectionID+"/date/"+$scope.date+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
            .success(function(data) {
			 //alert("SUCCESS");
			 console.log(data);
			$scope.defaultData = data;
			//$scope.$apply();
			//window.localStorage.setItem('studata', data);
            })
            .error(function(data) {
             $scope.defaultData = '';
			//$scope.radioData = '';
			//window.localStorage.setItem('studata', '');
            });

$scope.classattend= function(date){
	
	var date=new Date(date),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    $scope.date=[ date.getFullYear(), mnth, day ].join("-");
	console.log($scope.date);
 $http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getallattendance/classid/"+$scope.classID+"/sectionid/"+$scope.sectionID+"/date/"+$scope.date+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
            .success(function(data) {
			 //alert("SUCCESS");
			 console.log(data);
			$scope.defaultData = data;
		//	$scope.$apply();
			//window.localStorage.setItem('studata', data);
            })
            .error(function(data) {
             $scope.defaultData = '';
			//$scope.radioData = '';
			//window.localStorage.setItem('studata', '');
            });
};
$scope.studentsearch= function(studentID){
	$scope.studentID=studentID;
	 $http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getsingleattendance/studentid/"+$scope.studentID+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
            .success(function(data) {
			 //alert("SUCCESS");
			 console.log(data);
			$scope.studentname = data[0]['name'];
			$scope.classesname = data[0]['classname']+'-'+data[0]['sectionname'];
			$scope.percent = data[0]['percentage'];
			$scope.presentdays = data[0]['present'];
			$scope.absentdays = data[0]['absent'];
			//window.localStorage.setItem('studata', data);
            })
            .error(function(data) {
             $scope.studentname = '';
			$scope.classesname = '';
			$scope.percent = '';
			$scope.presentdays = '';
			$scope.absentdays = '';
			//$scope.radioData = '';
			//window.localStorage.setItem('studata', '');
            });
};
    // For show Grid Bottom Sheet.
    $scope.showGridBottomSheet = function ($event) {
        $mdBottomSheet.show({
            templateUrl: 'ui-grid-bottom-sheet-template',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showGridBottomSheet.


	$scope.doSecondaryAction = function(event) {
		$mdDialog.show(
		  $mdDialog.alert()
			.title('Secondary Action')
			.textContent('Secondary actions can be used for one click actions')
			.ariaLabel('Secondary click demo')
			.ok('Neat!')
			.targetEvent(event)
		);
	};


$scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.person, function (item) {
            person.Selected = $scope.selectedAll;
        });

    };
}
}); // End of  view attendance controller.

// Controller of assignment page.
appControllers.controller('assignmentCtrl', function ($scope, $state, $http,$mdDialog) {
	$scope.classID = localStorage.getItem("classID");
	$scope.sectionID = localStorage.getItem("sectionID");
	
if($scope.classID == '' || $scope.classID == null || $scope.sectionID =='' || $scope.sectionID == null )
{

$state.go("app.dashbroad");
}else{
	$scope.student='0';
	 $scope.type = "All";
	//$scope.typestu="All";
	//$scope.studentdatas= "";
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/student/class/" + $scope.classID+"/section/"+$scope.sectionID+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function(data) {
			console.log(data);
			$scope.studentdatas= data;
		})
		.error(function(data) {
			$scope.studentdatas= "";
		});
	$scope.radiochange = function (type) {
		//alert(type);
		
	if(type=='Specific'){
		$scope.typestu="Specific";
		
	$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/student/class/" + $scope.classID+"/section/"+$scope.sectionID+"?format=json";
		$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
		.success(function(data) {
			console.log(data);
			$scope.studentdatas= data;
		})
		.error(function(data) {
			$scope.studentdatas= "";
		});
	}else{
		$scope.typestu="";
		//$scope.typestu="All";
		$scope.studentdatas= "";
	}
	};

	$scope.assignsubmit = function (type,student,details) {
		//alert(type);
		//alert(student);
		//alert(details);
		var teacherID='1';
		if(student==null||student=="undefined"||type=='All'){
			student='0';
		}
		$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
		var url ="http://scoto.in/app/tturk/v1/api/staffAPI/insertassignment/";
		$http.get(url, { params: { "studentID": student, "details": details, "teacherID":teacherID, "type":type, "classID":$scope.classID, "sectionID":$scope.sectionID } })
		.success(function(data) {
			//alert("SUCCESS");
			console.log(data);
			$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'confirm-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: "Assignment Saved Successfully...!!!",
						content: "",
						ok: "OK"
					}
					}
					}).then(function () {
							$state.reload();
				});
			//$scope.datas= data;	
		})
		.error(function(data) {
				//$(".transaction_warning").html('<br><br><div style="color:red;font-size:20px;font-weight:600;text-align:center">Records Not Found...!!!</div>');
				//$(".transaction_content").css('display','none');
				$mdDialog.show({
				controller: 'DialogController',
				templateUrl: 'confirm-dialog.html',
				targetEvent: '',
				locals: {
					displayOption: {
						title: "Error...!!!",
						content: "",
						ok: "OK"
					}
					}
					}).then(function () {
							$state.reload();
				});
		});
	};
}
}); // End of assignment controller.

// Controller of view assignment page.
appControllers.controller('view_assignmentCtrl', function ($scope, $state, $http) {
//$scope.teacherid = localStorage.getItem("teacherid");
$scope.teacherid = '1';
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getteacherassignment/teacherid/" + $scope.teacherid;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
.success(function(data) {
	console.log(data);
	$scope.datas= data;	
})
.error(function(data) {
});
}); // End of view assignment controller.

// Controller of timetable page.
appControllers.controller('timetableCtrl', function ($scope, $state, $http) {
//$scope.teacherid = localStorage.getItem("teacherid");
$scope.teacherid = '1';
$http.defaults.headers.common['Authorization'] = 'Basic ' + btoa('admin' +  ':' + '1234');
var url ="http://scoto.in/app/tturk/v1/api/staffAPI/getstafftimetable/teacherid/" + $scope.teacherid;
$http.get(url, { params: { "key1": "value1", "key2": "value2" } })
.success(function(data) {
	console.log(data);
	$scope.datas= data;	
})
.error(function(data) {
});
}); // End of timetable controller.


// Controller of settlement page.
appControllers.controller('logoutCtrl', function ($scope,$rootScope,$timeout, $state) {
$rootScope = $rootScope.$new(true);
$scope = $scope.$new(true);
//alert('logout');
$state.go("app.login");
$state.reload();
}); // End of  settlement controller.







